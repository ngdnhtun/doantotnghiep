namespace Models.EF
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("BinhLuanDanhGia")]
    public partial class BinhLuanDanhGia
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(8)]
        public string maKH { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(5)]
        public string maSP { get; set; }

        [StringLength(1)]
        public string soSaoDanhGia { get; set; }

        [StringLength(200)]
        public string noiDungBinhLuan { get; set; }

        [Column(TypeName = "date")]
        public DateTime? ngayDanhGia { get; set; }

        public virtual KhachHang KhachHang { get; set; }

        public virtual SanPham SanPham { get; set; }
    }
}
