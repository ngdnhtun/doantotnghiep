namespace Models.EF
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SanPham")]
    public partial class SanPham
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public SanPham()
        {
            BinhLuanDanhGias = new HashSet<BinhLuanDanhGia>();
            ChiTietDonHangs = new HashSet<ChiTietDonHang>();
            ThongTinSanPhams = new HashSet<ThongTinSanPham>();
        }

        [Key]
        [StringLength(5)]
        public string maSP { get; set; }

        [StringLength(2)]
        public string maLSP { get; set; }

        [StringLength(80)]
        public string tenSP { get; set; }

        [StringLength(80)]
        public string giaBan { get; set; }

        public int? soLuong { get; set; }

        [Column(TypeName = "ntext")]
        public string moTa { get; set; }

        public int? xemNhieu { get; set; }

        public int? muaNhieu { get; set; }

        public double? tiLeDanhGia { get; set; }

        [StringLength(2)]
        public string baoHanh { get; set; }

        [StringLength(200)]
        public string anhSP { get; set; }

        [StringLength(200)]
        public string hinhAnh1 { get; set; }

        [StringLength(200)]
        public string hinhAnh2 { get; set; }

        [StringLength(200)]
        public string hinhAnh3 { get; set; }

        [StringLength(200)]
        public string hinhAnh4 { get; set; }

        [StringLength(200)]
        public string hinhAnh5 { get; set; }

        [StringLength(50)]
        public string tinhTrang { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BinhLuanDanhGia> BinhLuanDanhGias { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ChiTietDonHang> ChiTietDonHangs { get; set; }

        public virtual ChiTietSanPham ChiTietSanPham { get; set; }

        public virtual LoaiSanPham LoaiSanPham { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ThongTinSanPham> ThongTinSanPhams { get; set; }
    }
}
