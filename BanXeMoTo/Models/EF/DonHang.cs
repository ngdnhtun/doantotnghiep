namespace Models.EF
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("DonHang")]
    public partial class DonHang
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public DonHang()
        {
            ChiTietDonHangs = new HashSet<ChiTietDonHang>();
            PhieuXuats = new HashSet<PhieuXuat>();
        }

        [Key]
        [StringLength(9)]
        public string maDH { get; set; }

        [StringLength(8)]
        public string maKH { get; set; }

        [StringLength(10)]
        public string maNV { get; set; }

        [Column(TypeName = "date")]
        public DateTime? ngayDatHang { get; set; }

        [Column(TypeName = "date")]
        public DateTime? ngayGiaoHang { get; set; }

        [Column(TypeName = "date")]
        public DateTime? ngayThanhToan { get; set; }

        [StringLength(500)]
        public string diaChiGiaoHang { get; set; }

        public int? tongSoLuong { get; set; }

        [Column(TypeName = "money")]
        public decimal? tongPhu { get; set; }

        [Column(TypeName = "money")]
        public decimal? tongGiam { get; set; }

        [Column(TypeName = "money")]
        public decimal? tongTien { get; set; }

        [StringLength(50)]
        public string trangThai { get; set; }

        [Column(TypeName = "ntext")]
        public string lyDo { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ChiTietDonHang> ChiTietDonHangs { get; set; }

        public virtual KhachHang KhachHang { get; set; }

        public virtual NhanVien NhanVien { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PhieuXuat> PhieuXuats { get; set; }
    }
}
