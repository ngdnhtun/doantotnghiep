namespace Models.EF
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PhieuXuat")]
    public partial class PhieuXuat
    {
        [Key]
        public int idPX { get; set; }

        [StringLength(7)]
        public string maTTSP { get; set; }

        [StringLength(9)]
        public string maDH { get; set; }

        [StringLength(10)]
        public string maNV { get; set; }

        [Column(TypeName = "date")]
        public DateTime? ngayXuat { get; set; }

        public virtual DonHang DonHang { get; set; }

        public virtual NhanVien NhanVien { get; set; }

        public virtual ThongTinSanPham ThongTinSanPham { get; set; }
    }
}
