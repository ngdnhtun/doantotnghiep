namespace Models.EF
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PhieuNhap")]
    public partial class PhieuNhap
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public PhieuNhap()
        {
            ThongTinSanPhams = new HashSet<ThongTinSanPham>();
        }

        [Key]
        [StringLength(8)]
        public string maPN { get; set; }

        [StringLength(4)]
        public string maNCC { get; set; }

        [StringLength(10)]
        public string maNV { get; set; }

        [Column(TypeName = "date")]
        public DateTime? ngayNhap { get; set; }

        public int? soLuong { get; set; }

        [Column(TypeName = "money")]
        public decimal? tongTien { get; set; }

        public virtual NhaCungCap NhaCungCap { get; set; }

        public virtual NhanVien NhanVien { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ThongTinSanPham> ThongTinSanPhams { get; set; }
    }
}
