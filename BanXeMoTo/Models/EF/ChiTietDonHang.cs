namespace Models.EF
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ChiTietDonHang")]
    public partial class ChiTietDonHang
    {
        [Key]
        public int idCTDH { get; set; }

        [StringLength(9)]
        public string maDH { get; set; }

        [StringLength(5)]
        public string maSP { get; set; }

        [StringLength(50)]
        public string mauSac { get; set; }

        [Column(TypeName = "money")]
        public decimal? thanhTien { get; set; }

        public virtual DonHang DonHang { get; set; }

        public virtual SanPham SanPham { get; set; }
    }
}
