namespace Models.EF
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("GioHang")]
    public partial class GioHang
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int maGH { get; set; }

        [StringLength(8)]
        public string maKH { get; set; }

        [StringLength(5)]
        public string maSP { get; set; }

        [StringLength(80)]
        public string tenSP { get; set; }

        [StringLength(50)]
        public string mauSac { get; set; }

        [Column(TypeName = "money")]
        public decimal? giaBan { get; set; }

        [StringLength(200)]
        public string anhSP { get; set; }

        public virtual KhachHang KhachHang { get; set; }
    }
}
