namespace Models.EF
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ThongTinSanPham")]
    public partial class ThongTinSanPham
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ThongTinSanPham()
        {
            PhieuXuats = new HashSet<PhieuXuat>();
        }

        [Key]
        [StringLength(7)]
        public string maTTSP { get; set; }

        [StringLength(8)]
        public string maPN { get; set; }

        [StringLength(5)]
        public string maSP { get; set; }

        [StringLength(12)]
        public string soKhung { get; set; }

        [StringLength(12)]
        public string soMay { get; set; }

        [StringLength(50)]
        public string mauSac { get; set; }

        [Column(TypeName = "money")]
        public decimal? giaGoc { get; set; }

        [Column(TypeName = "money")]
        public decimal? giaBan { get; set; }

        [StringLength(50)]
        public string tinhTrang { get; set; }

        public virtual PhieuNhap PhieuNhap { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PhieuXuat> PhieuXuats { get; set; }

        public virtual SanPham SanPham { get; set; }
    }
}
