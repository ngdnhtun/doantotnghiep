namespace Models.EF
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ChiTietSanPham")]
    public partial class ChiTietSanPham
    {
        [Key]
        [StringLength(5)]
        public string maSP { get; set; }

        [StringLength(200)]
        public string dongCo { get; set; }

        [StringLength(200)]
        public string dungTichXilanh { get; set; }

        [StringLength(200)]
        public string dKhTPiston { get; set; }

        [StringLength(200)]
        public string tiLeNen { get; set; }

        [StringLength(200)]
        public string heThongNhienLieu { get; set; }

        [StringLength(200)]
        public string danhLua { get; set; }

        [StringLength(200)]
        public string truyenTai { get; set; }

        [StringLength(200)]
        public string giamSocBanhTruoc { get; set; }

        [StringLength(200)]
        public string giamSocBanhSau { get; set; }

        [StringLength(200)]
        public string lopTruoc { get; set; }

        [StringLength(200)]
        public string lopSau { get; set; }

        [StringLength(200)]
        public string phanhTruoc { get; set; }

        [StringLength(200)]
        public string phanhSau { get; set; }

        [StringLength(200)]
        public string khungXe { get; set; }

        [StringLength(200)]
        public string chieuDai { get; set; }

        [StringLength(200)]
        public string chieuRong { get; set; }

        [StringLength(200)]
        public string chieuCao { get; set; }

        [StringLength(200)]
        public string chieuCaoYen { get; set; }

        [StringLength(200)]
        public string trongLuongKho { get; set; }

        [StringLength(200)]
        public string thungNhienLieu { get; set; }

        [StringLength(200)]
        public string chieuDaiCoSo { get; set; }

        [StringLength(200)]
        public string tinhNangDacBiet { get; set; }

        public virtual SanPham SanPham { get; set; }
    }
}
