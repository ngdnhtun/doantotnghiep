﻿using Models.EF;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations.Builders;
using System.Linq;
using System.Security.Cryptography.Pkcs;
using System.Text;
using System.Threading.Tasks;

namespace Models.DAO
{
    public class UserDao
    {
        private WebDbContext db;

        public UserDao()
        {
            db = new WebDbContext();
        }

        #region Controller Login & Signup

        //Login
        public int Login(string user, string pass)
        {
            KhachHang result = db.KhachHangs.SingleOrDefault(x => (x.maKH.Equals(user) || x.SDT.Equals(user) || x.Email.Equals(user)) && x.matKhau.Equals(pass));
            if (result != null)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }

        //Tạo mã khách hàng tự tăng
        public string TaoMaKhachHang()
        {
            var list = db.KhachHangs.OrderByDescending(s => s.maKH).FirstOrDefault();
            string chuoi = list.maKH;
            string tachchuoi = chuoi.Substring(2, 6);
            int machuoi = Convert.ToInt32(tachchuoi);
            machuoi += 1;
            string makhachhang = "KH" + machuoi;
            return makhachhang;
        }

        //Đăng ký tài khoản khách hàng Signup
        public bool Insert(KhachHang user)
        {
            try
            {
                db.KhachHangs.Add(user);
                db.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        #endregion

        #region Controller Taikhoan

        //Lấy thông tin của 1 khách hàng
        public KhachHang InfoKhachHang(string user)
        {
            KhachHang result = db.KhachHangs.SingleOrDefault(x => (x.maKH.Equals(user) || x.SDT.Equals(user) || x.Email.Equals(user)));
            if (result != null)
            {
                return result;
            }
            else
            {
                return null;
            }
        }

        //Khách hàng đổi mật khẩu 
        public bool DoiMatKhau(string makh, string matkhau, string nickname)
        {
            var result = db.KhachHangs.Find(makh);
            try
            {
                if (result != null)
                {
                    result.matKhau = matkhau;
                    result.nickName = nickname;
                    db.SaveChanges();
                    return true;
                }
            }
            catch (Exception) { }
            return false;
        }

        //Khách hàng cập nhật thông tin 
        public bool CapNhatThongTin(KhachHang model)
        {
            var result = db.KhachHangs.Find(model.maKH);
            try
            {
                if (result != null)
                {
                    result.tenKH = model.tenKH;
                    result.gioiTinh = model.gioiTinh;
                    result.ngaySinh = model.ngaySinh;
                    result.diaChiKH = model.diaChiKH;
                    db.SaveChanges();
                    return true;
                }
            }
            catch (Exception) { }
            return false;
        }

        //Lấy danh sách thông tin đơn hàng chờ xử lý với mã khách hàng
        public List<DonHang> ListDonHang(string makh)
        {
            var dh = db.DonHangs.Where(x => x.maKH.Contains(makh) && !x.trangThai.Contains("Đã nhận") && !x.trangThai.Contains("Đã hủy"));
            return dh.OrderByDescending(x => x.maDH).ToList();
        }

        //Lấy danh sách tất cả thông tin đơn hàng với mã khách hàng
        public List<DonHang> ListDonHangAll(string makh)
        {
            var dh = db.DonHangs.Where(x => x.maKH.Contains(makh));
            return dh.OrderByDescending(x => x.maDH).ToList();
        }

        //Lấy danh sách thông tin chi tiết đơn hàng
        public List<ChiTietDonHang> ListChiTietDonHang(string madonhang)
        {
            var dh = db.ChiTietDonHangs.Where(x => x.maDH.Contains(madonhang));
            return dh.ToList();
        }

        //Lấy thông tin của 1 đơn hàng với mã đơn hàng
        public DonHang InfoDonHang(string madonhang)
        {
            var donhang = db.DonHangs.Where(x => x.maDH.Contains(madonhang)).FirstOrDefault();
            return donhang;
        }

        //Hủy đơn hàng
        public bool HuyDonHang(string madonhang)
        {
            try
            {
                var ctdh = db.ThongTinSanPhams.Where(x => x.tinhTrang.Contains(madonhang));
                foreach (var item in ctdh)
                {
                    item.tinhTrang = "Còn trong kho";
                }
                var dh = db.DonHangs.Where(x => x.maDH.Contains(madonhang)).FirstOrDefault();
                dh.trangThai = "Đã hủy";
                dh.lyDo = "Khách hàng đã hủy";
                db.SaveChanges();
                return true;
            }
            catch (Exception) { }
            return false;
        }

        #endregion

        #region Controller Sanpham

        //Lấy thông tin của 1 sản phẩm
        public SanPham InfoSanPham(string masanpham)
        {
            SanPham result = db.SanPhams.Where(x => string.IsNullOrEmpty(x.tinhTrang)).SingleOrDefault(x => x.maSP.Contains(masanpham));
            if (result != null)
            {
                return result;
            }
            else
            {
                return null;
            }
        }

        //Lấy tất cả sản phẩm và tự động cập nhật giá
        public List<SanPham> ListSanPhamAutoUpdate()
        {
            try
            {
                var model1 = db.SanPhams.Select(x => x.maSP);
                foreach (var item in model1)
                {
                    IQueryable<decimal?> giasp = db.ThongTinSanPhams.Where(x => x.maSP.Contains(item)).Select(x => x.giaBan);
                    decimal? giathap = giasp.OrderBy(x => x.Value).FirstOrDefault();
                    decimal? giacao = giasp.OrderByDescending(x => x.Value).FirstOrDefault();
                    SanPham model2 = db.SanPhams.Where(x => x.maSP.Contains(item)).FirstOrDefault();
                    if (giathap == null)
                    {
                        model2.giaBan = "Liên Hệ";
                    }
                    else
                    {
                        model2.giaBan = String.Format("{0:0,0}", giathap) + " - " + String.Format("{0:0,0}", giacao) + " VNĐ";
                    }
                }
                db.SaveChanges();
            }
            catch (Exception) { }
            IQueryable<SanPham> model = db.SanPhams.Where(x => string.IsNullOrEmpty(x.tinhTrang)).OrderByDescending(x => x.soLuong).ThenBy(x => x.tenSP);
            return model.ToList();
        }

        //Tìm sản phẩm theo hãng sản xuất
        public List<SanPham> ListSanPhamHSX(string hsx)
        {
            IQueryable<SanPham> model = db.SanPhams.Where(x => string.IsNullOrEmpty(x.tinhTrang)).OrderByDescending(x => x.soLuong).ThenBy(x => x.tenSP);
            model = model.Where(x => x.LoaiSanPham.maHSX.Contains(hsx));
            return model.ToList();
        }

        //Tìm sản phẩm ở thanh tìm kiếm trang chủ của người dùng
        public List<SanPham> ListWhereSanPhamAll(string searchString)
        {
            IQueryable<SanPham> model = db.SanPhams.Where(x => string.IsNullOrEmpty(x.tinhTrang));
            if (!string.IsNullOrEmpty(searchString))
            {
                model = model.Where(x => x.tenSP.Contains(searchString) || x.maSP.Contains(searchString) || x.LoaiSanPham.tenLSP.Contains(searchString) || x.LoaiSanPham.HangSanXuat.tenHSX.Contains(searchString));
            }
            return model.OrderBy(x => x.tenSP).ToList();
        }

        //Lấy danh sách sản phẩm theo loại sản phẩm
        public List<SanPham> ListLoaiSanPhamAll(string lsp)
        {
            IQueryable<SanPham> model = db.SanPhams.Where(x => string.IsNullOrEmpty(x.tinhTrang)).OrderByDescending(x => x.soLuong).ThenBy(x => x.tenSP);
            model = model.Where(x => x.maLSP.Contains(lsp));
            return model.ToList();
        }

        //Lấy bình luận đánh giá sản phẩm
        public List<BinhLuanDanhGia> BinhLuanDanhGia(string masanpham)
        {
            IQueryable<BinhLuanDanhGia> model = db.BinhLuanDanhGias;
            model = model.Where(x => x.maSP.Contains(masanpham)).OrderByDescending(x => x.ngayDanhGia).ThenByDescending(x => x.soSaoDanhGia);
            return model.Take(10).ToList();
        }

        //Kiểm tra đã từng mua hàng chưa thì mới đánh giá được
        public int KiemTraMuaHang(string makh, string masanpham)
        {
            IQueryable<DonHang> model = db.DonHangs.Where(x => x.maKH.Contains(makh));
            IQueryable<ChiTietDonHang> model2 = db.ChiTietDonHangs.Where(x => x.maSP.Contains(masanpham));
            foreach (var item in model)
            {
                foreach (var item2 in model2)
                {
                    if (item.maDH == item2.maDH)
                    {
                        return 1;
                    }
                }
            }
            return 0;
        }

        //Tạo bình luận đánh giá đơn hàng
        public int TaiBinhLuanDanhGia(BinhLuanDanhGia danhgia)
        {
            try
            {
                var dg = db.BinhLuanDanhGias.Where(x => x.maKH.Contains(danhgia.maKH) && x.maSP.Contains(danhgia.maSP)).FirstOrDefault();
                if (dg == null)
                {
                    db.BinhLuanDanhGias.Add(danhgia);
                    db.SaveChanges();
                    return 1;
                }
                else
                {
                    dg.soSaoDanhGia = danhgia.soSaoDanhGia;
                    dg.noiDungBinhLuan = danhgia.noiDungBinhLuan;
                    dg.ngayDanhGia = danhgia.ngayDanhGia;
                    db.SaveChanges();
                    return 2;
                }
            }
            catch (Exception) { }
            return 3;
        }

        //Lấy giá sản phẩm trên trang chi tiết
        public ThongTinSanPham LayGiaThongTinSanPham(string mausacchon)
        {
            ThongTinSanPham result = db.ThongTinSanPhams.Where(x => x.mauSac.Contains(mausacchon)).FirstOrDefault();
            if (result != null)
            {
                return result;
            }
            return null;
        }

        #endregion

        #region Controller Cart

        //Tính số lượng sản phẩm còn trong kho
        public int TinhSoLuong(string masanpham)
        {
            int tb = 0;
            try
            {
                var sosao = db.ThongTinSanPhams.Where(x => x.maSP.Contains(masanpham) && x.tinhTrang.Contains("Còn trong kho"));
                tb = sosao.Count();
                var model = db.SanPhams.Where(x => string.IsNullOrEmpty(x.tinhTrang) && x.maSP.Contains(masanpham)).SingleOrDefault(x => x.maSP.Contains(masanpham));
                model.soLuong = tb;
                db.SaveChanges();
            }
            catch (Exception) { }
            return tb;
        }

        //Lấy màu sắc của sản phẩm trong thông tin sản phẩm
        public ArrayList LayMauSac(string masanpham)
        {
            var li = new ArrayList();
            try
            {
                var sosao = db.ThongTinSanPhams.Where(x => x.maSP.Contains(masanpham) && x.tinhTrang.Contains("Còn trong kho")).Select(x => x.mauSac);
                sosao = sosao.Distinct(); //bỏ trùng lặp
                foreach (var item in sosao)
                {
                    li.Add(item);
                }
            }
            catch (Exception) { }
            return li;
        }

        //Lấy giá tiền của sản phẩm trong bảng thông tin sản phẩm
        public decimal? LayGiaBan(string masanpham, string mausac)
        {
            decimal? giaban = 0;
            giaban = db.ThongTinSanPhams.Where(x => x.maSP.Contains(masanpham) && x.mauSac.Contains(mausac) && x.tinhTrang.Contains("Còn trong kho")).Select(x => x.giaBan).FirstOrDefault();
            return giaban;
        }

        //Tính màu còn bao nhiêu trong bảng thông tin sản phẩm
        public int TinhMau(string masp, string chonmau)
        {
            var a = db.ThongTinSanPhams.Where(x => x.maSP.Contains(masp) && x.mauSac.Contains(chonmau) && x.tinhTrang.Contains("Còn trong kho"));
            return a.Count();
        }

        //Lưu địa chỉ nhận hàng của người mua khi chọn lưu
        public bool LuuDiaChi(string makh, string diachi)
        {
            try
            {
                var model = db.KhachHangs.Find(makh);
                model.diaChiKH = diachi;
                db.SaveChanges();
                return true;
            }
            catch (Exception) { }
            return false;
        }

        //Tạo mã đơn hàng tự tăng
        public string TaoMaDonHang()
        {
            var list = db.DonHangs.OrderByDescending(s => s.maDH).FirstOrDefault();
            string chuoi = list.maDH;
            var chuoi2 = Convert.ToInt32(chuoi);
            chuoi2 += 1;
            var madonhang = chuoi2;
            return madonhang.ToString();
        }

        //Tạo đơn hàng của khách hàng và lưu vào csdl
        public bool TaoDonHang(DonHang donhang)
        {
            try
            {
                db.DonHangs.Add(donhang);
                db.SaveChanges();
                return true;
            }
            catch (Exception) { }
            return false;
        }

        //Tạo chi tiết đơn hàng
        public bool TaoChiTietDonHang(ChiTietDonHang ctdh)
        {
            try
            {
                db.ChiTietDonHangs.Add(ctdh);
                db.SaveChanges();
                return true;
            }
            catch (Exception) { }
            return false;
        }

        //Cập nhật trang thái sản phẩm khi xác nhận mua hàng
        public bool UpdateTinhTrangThongTinSanPham(string masanpham, string mausac, string madonhang)
        {
            try
            {
                var ttsp = db.ThongTinSanPhams.Where(x => x.maSP.Contains(masanpham) && x.mauSac.Contains(mausac) && x.tinhTrang.Contains("Còn trong kho")).FirstOrDefault();
                ttsp.tinhTrang = madonhang;
                db.SaveChanges();
                return true;
            }
            catch (Exception) { }
            return false;
        }

        #endregion

        #region Controller Home

        //Lấy sản phẩm xem nhiều
        public List<SanPham> ListXemNhieu()
        {
            IQueryable<SanPham> model = db.SanPhams.Where(x => string.IsNullOrEmpty(x.tinhTrang));
            model = model.OrderByDescending(x => x.xemNhieu).Take(8);
            return model.ToList();
        }

        //Lấy sản phẩm mua nhiều
        public List<SanPham> ListMuaNhieu()
        {
            IQueryable<SanPham> model = db.SanPhams.Where(x => string.IsNullOrEmpty(x.tinhTrang));
            model = model.OrderByDescending(x => x.muaNhieu).Take(8);
            return model.ToList();
        }

        //Lấy sản phẩm ngẫu nhiên
        public List<SanPham> ListNgauNhien()
        {
            IQueryable<SanPham> model = db.SanPhams.Where(x => string.IsNullOrEmpty(x.tinhTrang));
            model = model.Where(x => x.soLuong > 0).OrderBy(x => Guid.NewGuid()).Take(8);
            return model.ToList();
        }

        //Lấy sản phẩm đánh giá cao
        public List<SanPham> ListDanhGiaCao()
        {
            IQueryable<SanPham> model = db.SanPhams.Where(x => string.IsNullOrEmpty(x.tinhTrang));
            model = model.OrderByDescending(x => x.tiLeDanhGia).Take(8);

            return model.ToList();
        }

        //Lấy sản 3 phẩm đánh giá cao cho index-home
        public List<SanPham> BaDanhGiaCao()
        {
            IQueryable<SanPham> model = db.SanPhams.Where(x => string.IsNullOrEmpty(x.tinhTrang));
            model = model.OrderByDescending(x => x.tiLeDanhGia).Take(3);
            return model.ToList();
        }

        //Lấy sản 3 phẩm đánh giá thấp cho index-home
        public List<SanPham> BaDanhGiaThap()
        {
            IQueryable<SanPham> model = db.SanPhams.Where(x => string.IsNullOrEmpty(x.tinhTrang));
            model = model.OrderBy(x => x.tiLeDanhGia).Take(3);
            return model.ToList();
        }

        #endregion

        #region Hàm khác

        //Tăng lượt xem sản phẩm
        public void TangLuotXem(string masanpham)
        {
            try
            {
                SanPham result = db.SanPhams.Where(x => string.IsNullOrEmpty(x.tinhTrang) && x.maSP.Contains(masanpham)).SingleOrDefault(x => x.maSP.Contains(masanpham));
                result.xemNhieu += 1;
                db.SaveChanges();
            }
            catch (Exception) { }
        }

        //Tăng lượt mua sản phẩm
        public void TangLuotMua(string masanpham)
        {
            try
            {
                SanPham result = db.SanPhams.Where(x => string.IsNullOrEmpty(x.tinhTrang) && x.maSP.Contains(masanpham)).SingleOrDefault(x => x.maSP.Contains(masanpham));
                result.muaNhieu += 1;
                db.SaveChanges();
            }
            catch (Exception) { }
        }

        //Tính tỉ lệ đánh giá của sản phẩm
        public double TinhTiLeDanhGia(string masanpham)
        {
            double tb = 0, tong = 0;
            try
            {
                var sosao = db.BinhLuanDanhGias.Where(x => x.maSP.Contains(masanpham)).Select(x => x.soSaoDanhGia);
                tb = sosao.Count();
                foreach (var item in sosao)
                {
                    var a = Convert.ToInt32(item);
                    tong += a;
                }
                var model = db.SanPhams.Where(x => string.IsNullOrEmpty(x.tinhTrang) && x.maSP.Contains(masanpham)).SingleOrDefault(x => x.maSP.Contains(masanpham));
                if (tb != 0)
                {
                    model.tiLeDanhGia = Math.Round((tong / tb) * 100 / 5, 0);
                }
                else
                {
                    model.tiLeDanhGia = 0;
                }
                db.SaveChanges();
            }
            catch (Exception) { }
            return tb;
        }

        //Lấy danh sách loại sản phẩm cho menu Partail
        public List<LoaiSanPham> ListLoaiSanPham()
        {
            var dh = db.LoaiSanPhams;
            return dh.ToList();
        }

        //Tạo mã giỏ hàng
        public int TaoMaGioHang()
        {
            GioHang gh = db.GioHangs.OrderByDescending(x => x.maGH).FirstOrDefault();
            if (gh == null)
            {
                return 1;
            }
            return gh.maGH + 1;
        }
        //Chèn sản phẩm vào giỏ hàng
        public void AddGioHangVaoTable(GioHang giohang)
        {
            db.GioHangs.Add(giohang);
            db.SaveChanges();
        }
        //Lấy giỏ hàng của khách hàng ra
        public bool InfoGioHang(string makh)
        {
            var gh = db.GioHangs.Where(x => x.maKH.Contains(makh)).FirstOrDefault();
            if (gh != null)
            {
                return true;
            }
            return false;
        }
        //Lấy giỏ hàng của khách hàng ra
        public List<GioHang> ListGioHang(string makh)
        {
            var gh = db.GioHangs.Where(x => x.maKH.Contains(makh));
            return gh.ToList();
        }
        //Kiểm tra maSP, tenSP, mauSac sản phẩm đó có chưa
        public bool CheckGioHang(string makh, string masp, string mausac)
        {
            var gh = db.GioHangs.Where(x => x.maKH.Equals(makh) && x.maSP.Equals(masp) && x.mauSac.Equals(mausac)).FirstOrDefault();
            if (gh == null)
            {
                return true;
            }
            return false;
        }
        //Xóa giỏ hàng
        public bool XoaGioHang(string makh, string masp, string mausac)
        {
            try
            {
                var gh = db.GioHangs.Where(x => x.maKH.Equals(makh) && x.maSP.Equals(masp) && x.mauSac.Equals(mausac)).FirstOrDefault();
                db.GioHangs.Remove(gh);
                db.SaveChanges();
                return true;
            }
            catch (Exception) { }
            return false;
        }
        //Xóa tất cả giỏ hàng
        public bool XoaGioHangALL(string makh)
        {
            try
            {
                var gh = db.GioHangs.Where(x => x.maKH.Equals(makh));
                foreach (var item in gh)
                {
                    db.GioHangs.Remove(item);
                }
                db.SaveChanges();
                return true;
            }
            catch (Exception) { }
            return false;
        }
        //cập nhật giỏ hàng của khách hàng
        public bool UpdateGioHang(string makh, string masp, string maucu, string maumoi, decimal? giaban)
        {
            try
            {
                var gh = db.GioHangs.Where(x => x.maKH.Equals(makh) && x.maSP.Equals(masp) && x.mauSac.Equals(maucu)).FirstOrDefault();
                gh.mauSac = maumoi;
                gh.giaBan = giaban;
                db.SaveChanges();
                return true;
            }
            catch (Exception) { }

            return false;
        }

        #endregion

    }
}
