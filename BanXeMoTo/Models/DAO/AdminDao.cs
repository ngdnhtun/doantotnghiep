﻿using Models.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Models.DAO
{
    public class AdminDao
    {
        private WebDbContext db;

        public AdminDao()
        {
            db = new WebDbContext();
        }

        #region Controller Login

        //Login
        public int Login(string user, string pass)
        {
            NhanVien result = db.NhanViens.SingleOrDefault(x => (x.maNV.Equals(user) || x.SDT.Equals(user) || x.Email.Equals(user)) && x.matKhau.Equals(pass));
            if (result != null)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }

        #endregion

        #region Controller Nhanvien

        //Lấy tất cả danh sách nhân viên và tìm kiếm
        public List<NhanVien> ListNhanVien()
        {
            IQueryable<NhanVien> model = db.NhanViens;
            return model.ToList();
        }
        public List<NhanVien> ListNhanVien(string searchString)
        {
            IQueryable<NhanVien> model = db.NhanViens;
            if (!string.IsNullOrEmpty(searchString))
            {
                model = model.Where(x => x.maNV.Contains(searchString) || x.tenNV.Contains(searchString) || x.ChucVu.tenCV.Contains(searchString) || x.trangThai.Contains(searchString));
            }
            return model.ToList();
        }

        //Lấy thông tin của 1 nhân viên
        public NhanVien InfoNhanVien(string user)
        {
            NhanVien result = db.NhanViens.SingleOrDefault(x => (x.maNV.Equals(user) || x.SDT.Equals(user) || x.Email.Equals(user)));
            if (result != null)
            {
                return result;
            }
            else
            {
                return null;
            }
        }

        //Tạo mã nhân viên tự tăng
        public string TaoMaNhanVien()
        {
            var list = db.NhanViens.Where(x => !x.maNV.Contains("admin")).OrderByDescending(s => s.maNV).FirstOrDefault();
            string chuoi = list.maNV;
            var chuoi2 = Convert.ToInt32(chuoi);
            chuoi2 += 1;
            var manhanvien = chuoi2;
            return manhanvien.ToString();
        }

        //Tạo nhân viên mới, insert vào csdl
        public string InsertNhanVien(NhanVien nhanvien)
        {
            try
            {
                db.NhanViens.Add(nhanvien);
                db.SaveChanges();
                return "done";
            }
            catch (Exception)
            {
                var sdt = db.NhanViens.Any(x => x.SDT.Equals(nhanvien.SDT));
                var email = db.NhanViens.Any(x => x.Email.Equals(nhanvien.Email));
                var cmnd = db.NhanViens.Any(x => x.CMND.Equals(nhanvien.CMND));
                if (sdt) { return "SDT"; }
                if (email) { return "Email"; }
                if (cmnd) { return "CMND"; }
                return "";
            }
        }

        //Khóa tài khoán nhân viên
        public bool KhoaNhanVien(string manhanvien)
        {
            try
            {
                NhanVien nv = InfoNhanVien(manhanvien);
                if (nv != null)
                {
                    nv.trangThai = "Khóa";
                    db.SaveChanges();
                    return true;
                }
            }
            catch (Exception) { }
            return false;
        }

        //Hủy khóa tài khoán nhân viên
        public bool HuyKhoaNhanVien(string manhanvien)
        {
            try
            {
                NhanVien nv = InfoNhanVien(manhanvien);
                if (nv != null)
                {
                    nv.trangThai = "";
                    db.SaveChanges();
                    return true;
                }
            }
            catch (Exception) { }
            return false;
        }

        //Kiểm tra mật khẩu nhân viên trong update
        public bool CheckPassNhanVien(string manhanvien, string pass)
        {
            NhanVien bn = db.NhanViens.Where(x => x.maNV.Equals(manhanvien)).SingleOrDefault();
            if (bn != null && bn.matKhau != pass)
            {
                return true;
            }
            return false;
        }

        //Kiểm tra thông tin thông tin nhân viên
        public NhanVien KiemTraNhanVien(string manhanvien, string user)
        {
            NhanVien result = db.NhanViens.Where(x => !x.maNV.Contains(manhanvien)).SingleOrDefault(x => (x.CMND.Equals(user) || x.SDT.Equals(user) || x.Email.Equals(user)));
            if (result != null)
            {
                return result;
            }
            else
            {
                return null;
            }
        }

        //Cập nhật nhân viên
        public string UpdateNhanVien(NhanVien nhanvien)
        {
            try
            {
                NhanVien nv = InfoNhanVien(nhanvien.maNV);
                nv.maCV = nhanvien.maCV;
                nv.tenNV = nhanvien.tenNV;
                nv.CMND = nhanvien.CMND;
                nv.diaChiNV = nhanvien.diaChiNV;
                nv.SDT = nhanvien.SDT;
                nv.Email = nhanvien.Email;
                nv.gioiTinh = nhanvien.gioiTinh;
                nv.ngaySinh = nhanvien.ngaySinh;
                nv.matKhau = nhanvien.matKhau;
                nv.nickName = nhanvien.nickName;
                nv.tienLuong = nhanvien.tienLuong;
                db.SaveChanges();
                return "done";
            }
            catch (Exception)
            {
                NhanVien nv = InfoNhanVien(nhanvien.maNV);
                var sdt = db.NhanViens.Any(x => x.SDT.Equals(nhanvien.SDT) && !x.maNV.Equals(nhanvien.maNV));
                var email = db.NhanViens.Any(x => x.Email.Equals(nhanvien.Email) && !x.maNV.Equals(nhanvien.maNV));
                var cmnd = db.NhanViens.Any(x => x.CMND.Equals(nhanvien.CMND) && !x.maNV.Equals(nhanvien.maNV));
                if (sdt) { return "SDT"; }
                if (email) { return "Email"; }
                if (cmnd) { return "CMND"; }
                return "";
            }
        }

        //Lấy danh sách các chức vụ của nhân viên
        public List<ChucVu> DanhSachChucVu()
        {
            var listchucvu = db.ChucVus;
            return listchucvu.ToList();
        }

        #endregion

        #region Controller Thanhvien

        //Lấy tất cả danh sách thành viên và tìm kiếm
        public List<KhachHang> ListThanhVien()
        {
            IQueryable<KhachHang> model = db.KhachHangs;
            return model.ToList();
        }
        public List<KhachHang> ListThanhVien(string searchString)
        {
            IQueryable<KhachHang> model = db.KhachHangs;
            if (!string.IsNullOrEmpty(searchString))
            {
                model = model.Where(x => x.maKH.Contains(searchString) || x.tenKH.Contains(searchString) || x.SDT.Contains(searchString) || x.Email.Contains(searchString) || x.trangThai.Contains(searchString));
            }
            return model.ToList();
        }

        //lấy thông tin của 1 thành viên
        public KhachHang InfoThanhVien(string user)
        {
            KhachHang result = db.KhachHangs.SingleOrDefault(x => (x.maKH.Equals(user) || x.SDT.Equals(user) || x.Email.Equals(user)));
            if (result != null)
            {
                return result;
            }
            else
            {
                return null;
            }
        }

        //Tạo mã thành viên tự tăng
        public string TaoMaThanhVien()
        {
            var list = db.KhachHangs.OrderByDescending(s => s.maKH).FirstOrDefault();
            string chuoi = list.maKH;
            string tachchuoi = chuoi.Substring(2, 6);
            int machuoi = Convert.ToInt32(tachchuoi);
            machuoi += 1;
            string makhachhang = "KH" + machuoi;
            return makhachhang;
        }

        //Tạo thành viên mới, insert vào csdl
        public string InsertThanhVien(KhachHang user)
        {
            try
            {
                db.KhachHangs.Add(user);
                db.SaveChanges();
                return "done";
            }
            catch (Exception)
            {
                var sdt = db.KhachHangs.Any(x => x.SDT.Equals(user.SDT));
                var email = db.KhachHangs.Any(x => x.Email.Equals(user.Email));
                if (sdt) { return "SDT"; }
                if (email) { return "Email"; }
                return "";
            }
        }

        //Khóa tài khoản thành viên
        public bool KhoaThanhVien(string mathanhvien)
        {
            try
            {
                KhachHang nv = InfoThanhVien(mathanhvien);
                if (nv != null)
                {
                    nv.trangThai = "Khóa";
                    db.SaveChanges();
                    return true;
                }
            }
            catch (Exception) { }
            return false;
        }

        //Hủy khóa tài khoản thành viên
        public bool HuyKhoaThanhVien(string mathanhvien)
        {
            try
            {
                KhachHang nv = InfoThanhVien(mathanhvien);
                if (nv != null)
                {
                    nv.trangThai = "";
                    db.SaveChanges();
                    return true;
                }
            }
            catch (Exception) { }
            return false;
        }

        //Kiểm tra mật khẩu thành viên trong update
        public bool CheckPassThanhVien(string mathanhvien, string pass)
        {
            KhachHang bn = db.KhachHangs.Where(x => x.maKH.Equals(mathanhvien)).SingleOrDefault();
            if (bn != null && bn.matKhau != pass)
            {
                return true;
            }
            return false;
        }

        //Kiểm tra thông tin thông tin thành viên
        public KhachHang KiemTraThanhVien(string mathanhvien, string user)
        {
            KhachHang result = db.KhachHangs.Where(x => !x.maKH.Contains(mathanhvien)).SingleOrDefault(x => x.SDT.Equals(user) || x.Email.Equals(user));
            if (result != null)
            {
                return result;
            }
            else
            {
                return null;
            }
        }

        //Cập nhật thành viên
        public string UpdateThanhVien(KhachHang thanhvien)
        {
            try
            {
                KhachHang nv = InfoThanhVien(thanhvien.maKH);
                nv.tenKH = thanhvien.tenKH;
                nv.diaChiKH = thanhvien.diaChiKH;
                nv.SDT = thanhvien.SDT;
                nv.Email = thanhvien.Email;
                nv.gioiTinh = thanhvien.gioiTinh;
                nv.ngaySinh = thanhvien.ngaySinh;
                nv.matKhau = thanhvien.matKhau;
                nv.nickName = thanhvien.nickName;
                nv.tienThuong = thanhvien.tienThuong;
                db.SaveChanges();
                return "done";
            }
            catch (Exception)
            {
                KhachHang nv = InfoThanhVien(thanhvien.maKH);
                var sdt = db.KhachHangs.Any(x => x.SDT.Equals(thanhvien.SDT) && !x.maKH.Equals(nv.maKH));
                var email = db.KhachHangs.Any(x => x.Email.Equals(thanhvien.Email) && !x.maKH.Equals(nv.maKH));
                if (sdt) { return "SDT"; }
                if (email) { return "Email"; }
                return "";
            }
        }

        #endregion

        #region Controller Account

        //Đổi mật khẩu tài khoản
        public bool DoiMatKhau(string manv, string matkhau, string nickname)
        {
            var result = db.NhanViens.Find(manv);
            try
            {
                if (result != null)
                {
                    result.matKhau = matkhau;
                    result.nickName = nickname;
                    db.SaveChanges();
                    return true;
                }
            }
            catch (Exception) { }
            return false;
        }

        //Cập nhật thông tin tài khoản
        public bool CapNhatThongTin(NhanVien model)
        {
            var result = db.NhanViens.Find(model.maNV);
            try
            {
                if (result != null)
                {
                    result.tenNV = model.tenNV;
                    result.gioiTinh = model.gioiTinh;
                    result.ngaySinh = model.ngaySinh;
                    result.diaChiNV = model.diaChiNV;
                    db.SaveChanges();
                    return true;
                }
            }
            catch (Exception) { }
            return false;
        }

        #endregion

        #region Controller Order

        //Lấy thông tin của 1 đơn hàng
        public DonHang InfoDonHang(string madonhang)
        {
            DonHang result = db.DonHangs.SingleOrDefault(x => x.maDH.Contains(madonhang));
            if (result != null)
            {
                return result;
            }
            else
            {
                return null;
            }
        }

        //lấy thông tin của chi tiết đơn hàng
        public List<ChiTietDonHang> ListChiTietDonHang(string madonhang)
        {
            var dh = db.ChiTietDonHangs.Where(x => x.maDH.Contains(madonhang));
            return dh.ToList();
        }

        //Lấy đơn hàng đang đặt và tìm kiếm
        public List<DonHang> ListDonHangDangDat()
        {
            IQueryable<DonHang> model = db.DonHangs.Where(x => x.trangThai.Contains("Chờ xử lý") || x.trangThai.Contains("Đã xác nhận")).OrderBy(x => x.trangThai);
            return model.ToList();
        }
        public List<DonHang> ListDonHangDangDat(string searchString)
        {
            IQueryable<DonHang> model = db.DonHangs.Where(x => x.trangThai.Contains("Chờ xử lý") || x.trangThai.Contains("Đã xác nhận")).OrderBy(x => x.trangThai);
            if (!string.IsNullOrEmpty(searchString))
            {
                model = model.Where(x => x.maDH.Contains(searchString) || x.KhachHang.tenKH.Contains(searchString) || x.trangThai.Contains(searchString));
            }
            return model.ToList();
        }

        //Lấy đơn hàng đang vận chuyển và tìm kiếm
        public List<DonHang> ListDonHangDangVanChuyen()
        {
            IQueryable<DonHang> model = db.DonHangs.Where(x => x.trangThai.Contains("Đang vận chuyển"));
            return model.ToList();
        }
        public List<DonHang> ListDonHangDangVanChuyen(string searchString)
        {
            IQueryable<DonHang> model = db.DonHangs.Where(x => x.trangThai.Contains("Đang vận chuyển"));
            if (!string.IsNullOrEmpty(searchString))
            {
                model = model.Where(x => x.maDH.Contains(searchString) || x.KhachHang.tenKH.Contains(searchString) || x.trangThai.Contains(searchString));
            }
            return model.ToList();
        }

        //lấy tất cả đơn hàng và tìm kiếm
        public List<DonHang> ListDonHang()
        {
            IQueryable<DonHang> model = db.DonHangs.Where(x => !x.maDH.Contains("147000000")).OrderByDescending(x=>x.maDH);
            return model.ToList();
        }
        public List<DonHang> ListDonHang(string searchString)
        {
            IQueryable<DonHang> model = db.DonHangs.Where(x => !x.maDH.Contains("147000000"));
            if (!string.IsNullOrEmpty(searchString))
            {
                model = model.Where(x => x.maDH.Contains(searchString) || x.KhachHang.tenKH.Contains(searchString) || x.trangThai.Contains(searchString));
            }
            return model.ToList();
        }

        //Lấy tất cả đơn hàng đã nhận và tìm kiếm
        public List<DonHang> ListDonHangDaNhan()
        {
            IQueryable<DonHang> model = db.DonHangs.Where(x => x.trangThai.Contains("Đã nhận"));
            return model.ToList();
        }
        public List<DonHang> ListDonHangDaNhan(string searchString)
        {
            IQueryable<DonHang> model = db.DonHangs.Where(x => x.trangThai.Contains("Đã nhận"));
            if (!string.IsNullOrEmpty(searchString))
            {
                model = model.Where(x => x.maDH.Contains(searchString) || x.KhachHang.tenKH.Contains(searchString) || x.trangThai.Contains(searchString));
            }
            return model.ToList();
        }

        //lấy tất cả đơn hàng đã hủy và tìm kiếm
        public List<DonHang> ListDonHangDaHuy()
        {
            IQueryable<DonHang> model = db.DonHangs.Where(x => x.trangThai.Contains("Đã hủy"));
            return model.ToList();
        }
        public List<DonHang> ListDonHangDaHuy(string searchString)
        {
            IQueryable<DonHang> model = db.DonHangs.Where(x => x.trangThai.Contains("Đã hủy"));
            if (!string.IsNullOrEmpty(searchString))
            {
                model = model.Where(x => x.maDH.Contains(searchString) || x.KhachHang.tenKH.Contains(searchString) || x.trangThai.Contains(searchString));
            }

            return model.ToList();
        }

        //Cập nhật tình trạng đơn hàng
        public bool UpdateDonHang(string madonhang, string trangthai, string lydo, string manhanvien)
        {
            try
            {
                DonHang dh = InfoDonHang(madonhang);
                if (dh != null)
                {
                    if (trangthai == "1")
                    {
                        dh.trangThai = "Đã xác nhận";
                        var ttsp = db.ThongTinSanPhams.Where(x => x.tinhTrang.Contains(madonhang)).Select(x => x.maTTSP);
                        var a = ttsp.Count();
                        foreach (var item in ttsp)
                        {
                            var pxadd = new PhieuXuat();
                            pxadd.maTTSP = item;
                            pxadd.maDH = madonhang;
                            db.PhieuXuats.Add(pxadd);
                        }
                    }
                    else if (trangthai == "2")
                    {
                        //Nếu hủy đơn hàng
                        var ctdh = db.ThongTinSanPhams.Where(x => x.tinhTrang.Contains(madonhang));
                        foreach (var item in ctdh)
                        {
                            item.tinhTrang = "Còn trong kho";
                        }
                        var don = db.DonHangs.Where(x => x.maDH.Contains(madonhang)).FirstOrDefault();
                        don.trangThai = "Đã hủy";
                        don.lyDo = lydo;
                    }
                }
                dh.maNV = manhanvien;
                db.SaveChanges();
                return true;
            }
            catch (Exception) { }
            return false;
        }

        //Cập nhật trạng thái đơn hàng khi đơn hàng đã nhận
        public bool XacNhanDonHangDaNhan(string madonhang, string manhanvien)
        {
            try
            {
                DonHang dh = InfoDonHang(madonhang);
                if (dh != null)
                {
                    //hủy đơn hàng
                    var ctdh = db.ThongTinSanPhams.Where(x => x.tinhTrang.Contains(madonhang));
                    foreach (var item in ctdh)
                    {
                        item.tinhTrang = "Đã bán";
                    }
                    var don = db.DonHangs.Where(x => x.maDH.Contains(madonhang)).FirstOrDefault();
                    don.trangThai = "Đã nhận";
                }
                dh.maNV = manhanvien;
                dh.ngayThanhToan = DateTime.Now;
                db.SaveChanges();
                return true;
            }
            catch (Exception) { }
            return false;
        }

        #endregion

        #region Controller Khohang

        //Lấy đơn hàng đã xuất kho và tìm kiếm
        public List<DonHang> ListDonHangDaXuatKho()
        {
            IQueryable<DonHang> model = db.DonHangs.Where(x => x.trangThai.Contains("Đang vận chuyển") || x.trangThai.Contains("Đã nhận"));
            return model.ToList();
        }
        public List<DonHang> ListDonHangDaXuatKho(string searchString)
        {
            IQueryable<DonHang> model = db.DonHangs.Where(x => x.trangThai.Contains("Đang vận chuyển") || x.trangThai.Contains("Đã nhận"));
            if (!string.IsNullOrEmpty(searchString))
            {
                model = model.Where(x => x.maDH.Contains(searchString) || x.KhachHang.tenKH.Contains(searchString) || x.trangThai.Contains(searchString));
            }
            return model.ToList();
        }

        //Cập nhật lại thông tin sản phẩm trong phiếu nhập khi nhập
        public bool UpdateThongTinSanPhamPN(ThongTinSanPham ttsp)
        {
            try
            {
                ThongTinSanPham nv = InfoThongTinSanPham(ttsp.maTTSP);
                if (nv != null)
                {
                    nv.maSP = ttsp.maSP;
                    nv.soKhung = ttsp.soKhung;
                    nv.soMay = ttsp.soMay;
                    nv.mauSac = ttsp.mauSac;
                    nv.giaGoc = ttsp.giaGoc;
                    nv.giaBan = ttsp.giaGoc + (ttsp.giaGoc * 10 / 100);
                    nv.tinhTrang = "Còn trong kho";
                    db.SaveChanges();
                    return true;
                }
            }
            catch (Exception) { }
            return false;
        }

        //Lấy danh sách phiếu nhập và tìm kiếm
        public List<PhieuNhap> ListPhieuNhap()
        {
            IQueryable<PhieuNhap> model = db.PhieuNhaps;
            return model.ToList();
        }
        public List<PhieuNhap> ListPhieuNhap(string searchString)
        {
            IQueryable<PhieuNhap> model = db.PhieuNhaps;
            if (!string.IsNullOrEmpty(searchString))
            {
                model = model.Where(x => x.maPN.Contains(searchString) || x.NhaCungCap.tenNCC.Contains(searchString) || x.NhanVien.tenNV.Contains(searchString));
            }
            return model.ToList();
        }

        //Lấy danh sách phiếu xuất và tìm kiếm
        public List<PhieuXuat> ListPhieuXuat()
        {
            IQueryable<PhieuXuat> model = db.PhieuXuats;
            return model.ToList();
        }
        public List<PhieuXuat> ListPhieuXuat(string madonhang)
        {
            IQueryable<PhieuXuat> model = db.PhieuXuats.Where(x => x.maDH.Contains(madonhang));
            return model.ToList();
        }

        //Lấy thông tin của 1 phiếu nhập
        public PhieuNhap InfoPhieuNhap(string maphieunhap)
        {
            PhieuNhap result = db.PhieuNhaps.SingleOrDefault(x => x.maPN.Contains(maphieunhap));
            if (result != null)
            {
                return result;
            }
            else
            {
                return null;
            }
        }

        //Tính số lượng sản phẩm trong phiếu nhập
        public int TinhSoLuongPhieuNhap(string maphieunhap)
        {
            int tb = 0;
            try
            {
                var sosao = db.ThongTinSanPhams.Where(x => x.maPN.Contains(maphieunhap));
                tb = sosao.Count();
                var model = db.PhieuNhaps.Find(maphieunhap);
                model.soLuong = tb;
                db.SaveChanges();
            }
            catch (Exception) { }
            return tb;
        }

        //Tính tiền của phiếu nhập khi thêm hoặc cập nhật sản phẩm
        public decimal? TinhTienPhieuNhap(string maphieunhap)
        {
            decimal? tb = 0;
            try
            {
                var sosao = db.ThongTinSanPhams.Where(x => x.maPN.Contains(maphieunhap));
                tb = sosao.Sum(x => x.giaGoc);
                var model = db.PhieuNhaps.Find(maphieunhap);
                model.tongTien = tb;
                db.SaveChanges();
            }
            catch (Exception) { }
            return tb;
        }

        //Lấy danh sách các nhà cung cấp
        public List<NhaCungCap> DanhSachNhaCungCap()
        {
            var listncc = db.NhaCungCaps;
            return listncc.ToList();
        }

        //Tạo mã phiếu nhập tự tăng
        public string TaoMaPhieuNhap()
        {
            var list = db.PhieuNhaps.OrderByDescending(s => s.maPN).FirstOrDefault();
            string chuoi = list.maPN;
            string tachchuoi = chuoi.Substring(2, 6);
            int machuoi = Convert.ToInt32(tachchuoi);
            machuoi += 1;
            string mapn = "PN" + machuoi;
            return mapn;
        }

        //Tạo mã thông tin sản phẩm tự tăng
        public string TaoMaThongTinSanPham()
        {
            var list = db.ThongTinSanPhams.OrderByDescending(s => s.maTTSP).FirstOrDefault();
            string chuoi = list.maTTSP;
            var chuoi2 = Convert.ToInt32(chuoi);
            chuoi2 += 1;
            var mattsp = chuoi2;
            return mattsp.ToString();
        }

        //Tạo phiếu nhập mới, insert phiếu nhập
        public bool InsertPhieuNhap(PhieuNhap phieunhap)
        {
            try
            {
                db.PhieuNhaps.Add(phieunhap);
                db.SaveChanges();
                return true;
            }
            catch (Exception) { }
            return false;
        }

        //Lấy mã phiếu nhập cuối cùng để kiểm tra có cho nhập sản phẩm tiếp hay ko
        public string LayMaPhieuNhapCuoi()
        {
            DateTime daynow = DateTime.Now.AddDays(-1);
            PhieuNhap list = db.PhieuNhaps.Where(x => x.ngayNhap >= daynow).OrderByDescending(s => s.maPN).FirstOrDefault();
            if (list == null)
            {
                return "";
            }
            return list.maPN;
        }

        //Thêm thông tin sản phẩm vào csdl
        public string InsertThongTinSanPham(ThongTinSanPham ttsp)
        {
            try
            {
                ttsp.tinhTrang = "Còn trong kho";
                ttsp.giaBan = ttsp.giaGoc + (ttsp.giaGoc * 10 / 100);

                db.ThongTinSanPhams.Add(ttsp);
                db.SaveChanges();
                return "done";
            }
            catch (Exception)
            {
                var sokhung = db.ThongTinSanPhams.Any(x => x.soKhung.Equals(ttsp.soKhung));
                var somay = db.ThongTinSanPhams.Any(x => x.soMay.Equals(ttsp.soMay));
                if (sokhung) { return "soKhung"; }
                if (somay) { return "soMay"; }
                return "";
            }
        }

        //Xuất thông tin sản phẩm của sản phẩm đã xác nhận và tìm kiếm
        public List<DonHang> ListDonHangDaXacNhan()
        {
            IQueryable<DonHang> model = db.DonHangs.Where(x => x.trangThai.Contains("Đã xác nhận"));
            return model.ToList();
        }
        public List<DonHang> ListDonHangDaXacNhan(string searchString)
        {
            IQueryable<DonHang> model = db.DonHangs.Where(x => x.trangThai.Contains("Đã xác nhận"));
            if (!string.IsNullOrEmpty(searchString))
            {
                model = model.Where(x => x.maDH.Contains(searchString) || x.KhachHang.tenKH.Contains(searchString));
            }
            return model.ToList();
        }

        //Cập nhật trạng thái xuất đơn hàng trong thông tin sản phẩm
        public bool XacNhanXuatDonHang(string madonhang, string manhanvien)
        {
            try
            {
                DonHang dh = InfoDonHang(madonhang);
                if (dh != null)
                {
                    dh.trangThai = "Đang vận chuyển";
                    var ttsp = db.ThongTinSanPhams.Where(x => x.tinhTrang.Contains(madonhang)).Select(x => x.maTTSP);
                    var px = db.PhieuXuats.Where(x => x.maDH.Contains(madonhang));
                    var ngayxuat = DateTime.Now;
                    foreach (var item in px)
                    {
                        item.maNV = manhanvien;
                        item.ngayXuat = ngayxuat;
                    }
                }
                db.SaveChanges();
                return true;
            }
            catch (Exception) { }
            return false;
        }

        #endregion

        #region Controller Sanpham

        //Lấy tất cả danh sách sản phẩm và tìm kiếm
        public List<SanPham> ListSanPham()
        {
            IQueryable<SanPham> model = db.SanPhams;
            return model.ToList();
        }
        public List<SanPham> ListSanPham(string searchString)
        {
            IQueryable<SanPham> model = db.SanPhams;
            if (!string.IsNullOrEmpty(searchString))
            {
                model = model.Where(x => x.maSP.Contains(searchString) || x.tenSP.Contains(searchString) || x.giaBan.Contains(searchString) || x.LoaiSanPham.HangSanXuat.tenHSX.Contains(searchString) || x.LoaiSanPham.tenLSP.Contains(searchString) || x.tinhTrang.Contains(searchString));
            }
            return model.ToList();
        }

        //Lấy tất cả sản phẩm và tự động cập nhật giá
        public bool ListSanPhamAutoUpdate()
        {
            //Cập nhật số lượng của sản phẩm
            var tsl = ListSanPham();
            foreach (var item in tsl)
            {
                TinhSoLuong(item.maSP);
            }
            //Cập nhật giá của sản phẩm
            try
            {
                var model1 = db.SanPhams.Select(x => x.maSP);
                foreach (var item in model1)
                {
                    IQueryable<decimal?> giasp = db.ThongTinSanPhams.Where(x => x.maSP.Contains(item)).Select(x => x.giaBan);
                    decimal? giathap = giasp.OrderBy(x => x.Value).FirstOrDefault();
                    decimal? giacao = giasp.OrderByDescending(x => x.Value).FirstOrDefault();
                    SanPham model2 = db.SanPhams.Where(x => x.maSP.Contains(item)).FirstOrDefault();
                    if (giathap == null)
                    {
                        model2.giaBan = "Liên Hệ";
                    }
                    else
                    {
                        model2.giaBan = String.Format("{0:0,0}", giathap) + " - " + String.Format("{0:0,0}", giacao) + " VNĐ";
                    }
                }
                db.SaveChanges();
                return true;
            }
            catch (Exception) { }
            return false;
        }

        //Lấy thông tin của 1 sản phẩm
        public SanPham InfoSanPham(string masanpham)
        {
            SanPham result = db.SanPhams.SingleOrDefault(x => x.maSP.Contains(masanpham));
            if (result != null)
            {
                return result;
            }
            else
            {
                return null;
            }
        }

        //Cập nhật sản phẩm và chi tiết sản phẩm
        public bool UpdateSanPham(SanPham sanpham)
        {
            try
            {
                SanPham result = db.SanPhams.SingleOrDefault(x => x.maSP.Contains(sanpham.maSP));
                if (result != null)
                {
                    result.tenSP = sanpham.tenSP;
                    result.maLSP = sanpham.maLSP;
                    result.moTa = sanpham.moTa;
                    result.baoHanh = sanpham.baoHanh;
                    result.anhSP = sanpham.anhSP;
                    result.hinhAnh1 = sanpham.hinhAnh1;
                    result.hinhAnh2 = sanpham.hinhAnh2;
                    result.hinhAnh3 = sanpham.hinhAnh3;
                    result.hinhAnh4 = sanpham.hinhAnh4;
                    result.hinhAnh5 = sanpham.hinhAnh5;
                    result.ChiTietSanPham.dongCo = sanpham.ChiTietSanPham.dongCo;
                    result.ChiTietSanPham.dungTichXilanh = sanpham.ChiTietSanPham.dungTichXilanh;
                    result.ChiTietSanPham.dKhTPiston = sanpham.ChiTietSanPham.dKhTPiston;
                    result.ChiTietSanPham.tiLeNen = sanpham.ChiTietSanPham.tiLeNen;
                    result.ChiTietSanPham.heThongNhienLieu = sanpham.ChiTietSanPham.heThongNhienLieu;
                    result.ChiTietSanPham.danhLua = sanpham.ChiTietSanPham.danhLua;
                    result.ChiTietSanPham.giamSocBanhTruoc = sanpham.ChiTietSanPham.giamSocBanhTruoc;
                    result.ChiTietSanPham.giamSocBanhSau = sanpham.ChiTietSanPham.giamSocBanhSau;
                    result.ChiTietSanPham.lopTruoc = sanpham.ChiTietSanPham.lopTruoc;
                    result.ChiTietSanPham.lopSau = sanpham.ChiTietSanPham.lopSau;
                    result.ChiTietSanPham.phanhTruoc = sanpham.ChiTietSanPham.phanhTruoc;
                    result.ChiTietSanPham.phanhSau = sanpham.ChiTietSanPham.phanhSau;
                    result.ChiTietSanPham.khungXe = sanpham.ChiTietSanPham.khungXe;
                    result.ChiTietSanPham.chieuDai = sanpham.ChiTietSanPham.chieuDai;
                    result.ChiTietSanPham.chieuRong = sanpham.ChiTietSanPham.chieuRong;
                    result.ChiTietSanPham.chieuCao = sanpham.ChiTietSanPham.chieuCao;
                    result.ChiTietSanPham.chieuCaoYen = sanpham.ChiTietSanPham.chieuCaoYen;
                    result.ChiTietSanPham.trongLuongKho = sanpham.ChiTietSanPham.trongLuongKho;
                    result.ChiTietSanPham.thungNhienLieu = sanpham.ChiTietSanPham.thungNhienLieu;
                    result.ChiTietSanPham.chieuDaiCoSo = sanpham.ChiTietSanPham.chieuDaiCoSo;
                    result.ChiTietSanPham.tinhNangDacBiet = sanpham.ChiTietSanPham.tinhNangDacBiet;
                    db.SaveChanges();
                    return true;
                }
            }
            catch (Exception) { }
            return false;
        }

        //Tạo mã sản phẩm tự tăng
        public string TaoMaSanPham()
        {
            var list = db.SanPhams.OrderByDescending(s => s.maSP).FirstOrDefault();
            string chuoi = list.maSP;
            int machuoi = Convert.ToInt32(chuoi);
            machuoi += 1;
            return machuoi.ToString();
        }

        //Thêm sản phẩm và chi tiết sản phẩm
        public bool InsertSanPham(SanPham sanpham)
        {
            try
            {
                db.SanPhams.Add(sanpham);
                db.ChiTietSanPhams.Add(sanpham.ChiTietSanPham);
                db.SaveChanges();
                return true;
            }
            catch (Exception) { }
            return false;
        }

        //Lấy danh sách của thông tin sản phẩm và tìm kiếm
        public List<ThongTinSanPham> ListThongTinSanPham()
        {
            IQueryable<ThongTinSanPham> model = db.ThongTinSanPhams;
            return model.ToList();
        }
        public List<ThongTinSanPham> ListThongTinSanPham(string searchString)
        {
            IQueryable<ThongTinSanPham> model = db.ThongTinSanPhams;
            if (!string.IsNullOrEmpty(searchString))
            {
                model = model.Where(x => x.maTTSP.Contains(searchString) || x.maSP.Contains(searchString) || x.SanPham.tenSP.Contains(searchString) || x.soKhung.Contains(searchString) || x.soMay.Contains(searchString) || x.mauSac.Contains(searchString) || x.tinhTrang.Contains(searchString));
            }
            return model.ToList();
        }

        //Lấy thông tin thông tin của 1 thông tin sản phẩm
        public ThongTinSanPham InfoThongTinSanPham(string mattsp)
        {
            ThongTinSanPham result = db.ThongTinSanPhams.SingleOrDefault(x => x.maTTSP.Contains(mattsp));
            if (result != null)
            {
                return result;
            }
            else
            {
                return null;
            }
        }

        //Cập nhật thông tin của thông tin sản phẩm
        public string UpdateThongTinSanPham(ThongTinSanPham ttsp)
        {
            try
            {
                ThongTinSanPham nv = InfoThongTinSanPham(ttsp.maTTSP);
                nv.maSP = ttsp.maSP;
                nv.soKhung = ttsp.soKhung;
                nv.soMay = ttsp.soMay;
                nv.mauSac = ttsp.mauSac;
                nv.giaBan = ttsp.giaBan;
                nv.tinhTrang = ttsp.tinhTrang;
                db.SaveChanges();
                return "done";
            }
            catch (Exception)
            {
                ThongTinSanPham nv = InfoThongTinSanPham(ttsp.maTTSP);
                var sokhung = db.ThongTinSanPhams.Any(x => x.soKhung.Equals(ttsp.soKhung) && !x.maTTSP.Equals(nv.maTTSP));
                var somay = db.ThongTinSanPhams.Any(x => x.soMay.Equals(ttsp.soMay) && !x.maTTSP.Equals(nv.maTTSP));
                if (sokhung) { return "soKhung"; }
                if (somay) { return "soMay"; }
                return "";
            }
        }

        //Không bán sản phẩm
        public bool KhongBanSanPham(string masanpham)
        {
            try
            {
                SanPham nv = InfoSanPham(masanpham);
                if (nv != null)
                {
                    nv.tinhTrang = "Không bán";
                    db.SaveChanges();
                    return true;
                }
            }
            catch (Exception) { }
            return false;
        }

        //Hủy không bán sản phẩm
        public bool HuyKhongBanSanPham(string masanpham)
        {
            try
            {
                SanPham nv = InfoSanPham(masanpham);
                if (nv != null)
                {
                    nv.tinhTrang = "";
                    db.SaveChanges();
                    return true;
                }
            }
            catch (Exception) { }
            return false;
        }

        //Lấy danh sách loại sản phẩm
        public List<LoaiSanPham> ListLoaiSanPham()
        {
            IQueryable<LoaiSanPham> model = db.LoaiSanPhams;
            return model.ToList();
        }

        //Lấy danh sách hãng sản xuất
        public List<HangSanXuat> ListHangSanXuat()
        {
            IQueryable<HangSanXuat> model = db.HangSanXuats;
            return model.ToList();
        }

        #endregion

        #region Controller Report

        //Lấy tất cả đơn hàng theo tháng, năm
        public List<DonHang> ReportDonHang(int thang, int nam)
        {
            if (thang == 0)
            {
                IQueryable<DonHang> model = db.DonHangs.Where(x => !x.maDH.Contains("147000000") && x.ngayDatHang.Value.Year.Equals(nam));
                return model.ToList();
            }
            else
            {
                IQueryable<DonHang> model = db.DonHangs.Where(x => !x.maDH.Contains("147000000") && x.ngayDatHang.Value.Month.Equals(thang) && x.ngayDatHang.Value.Year.Equals(nam));
                return model.ToList();
            }
        }

        //Lấy tất cả phiếu nhập theo tháng, năm
        public List<PhieuNhap> ReportNhap(int thang, int nam)
        {
            if (thang == 0)
            {
                IQueryable<PhieuNhap> model = db.PhieuNhaps.Where(x => x.ngayNhap.Value.Year.Equals(nam));
                return model.ToList();
            }
            else
            {
                IQueryable<PhieuNhap> model = db.PhieuNhaps.Where(x => x.ngayNhap.Value.Month.Equals(thang) && x.ngayNhap.Value.Year.Equals(nam));
                return model.ToList();
            }
        }

        //Lấy tất cả phiếu xuất theo tháng, năm
        public List<PhieuXuat> ReportXuat(int thang, int nam)
        {
            if (thang == 0)
            {
                IQueryable<PhieuXuat> model = db.PhieuXuats.Where(x => x.ngayXuat.Value.Year.Equals(nam));
                return model.ToList();
            }
            else
            {
                IQueryable<PhieuXuat> model = db.PhieuXuats.Where(x => x.ngayXuat.Value.Month.Equals(thang) && x.ngayXuat.Value.Year.Equals(nam));
                return model.ToList();
            }
        }

        //Tính tổng giá trị giá gốc sản phẩm ban đầu (tổng giá trị nhập sản phẩm) theo tháng, năm
        public decimal? TongGiaTriSanPham(int thang, int nam)
        {
            decimal? gt = 0;
            IQueryable<DonHang> model = db.DonHangs.Where(x => !x.maDH.Contains("147000000"));
            if (thang == 0)
            {
                model = model.Where(x => x.ngayDatHang.Value.Year.Equals(nam)).Where(x => x.trangThai.Contains("Đã nhận"));
            }
            else
            {
                model = model.Where(x => x.ngayDatHang.Value.Month.Equals(thang) && x.ngayDatHang.Value.Year.Equals(nam)).Where(x => x.trangThai.Contains("Đã nhận"));
            }
            foreach (var item in model)
            {
                foreach (var item2 in item.PhieuXuats)
                {
                    gt += db.ThongTinSanPhams.Where(x => x.maTTSP.Contains(item2.maTTSP)).Sum(x => x.giaGoc);
                }
            }
            if (gt == null)
            {
                return 0;
            }
            return gt;
        }

        #endregion

        #region Controller Manage

        //Lấy danh sách nhà cung cấp và tìm kiếm
        public List<NhaCungCap> ListNhaCungCap()
        {
            IQueryable<NhaCungCap> model = db.NhaCungCaps;
            return model.ToList();
        }
        public List<NhaCungCap> ListNhaCungCap(string searchString)
        {
            IQueryable<NhaCungCap> model = db.NhaCungCaps;
            if (!string.IsNullOrEmpty(searchString))
            {
                model = model.Where(x => x.maNCC.Contains(searchString) || x.tenNCC.Contains(searchString) || x.diaChiNCC.Contains(searchString) || x.SDT.Contains(searchString) || x.nhanVienLienHe.Contains(searchString));
            }
            return model.ToList();
        }

        //Lấy danh sách chức vụ và tìm kiếm
        public List<ChucVu> ListChucVu()
        {
            IQueryable<ChucVu> model = db.ChucVus;
            return model.ToList();
        }
        public List<ChucVu> ListChucVu(string searchString)
        {
            IQueryable<ChucVu> model = db.ChucVus;
            if (!string.IsNullOrEmpty(searchString))
            {
                model = model.Where(x => x.maCV.Contains(searchString) || x.tenCV.Contains(searchString) || x.quyenCV.Contains(searchString));
            }
            return model.ToList();
        }

        //Lấy danh sách bình buận đánh giá và tìm kiếm
        public List<BinhLuanDanhGia> ListBinhLuanDanhGia()
        {
            IQueryable<BinhLuanDanhGia> model = db.BinhLuanDanhGias;
            return model.ToList();
        }
        public List<BinhLuanDanhGia> ListBinhLuanDanhGia(string searchString)
        {
            IQueryable<BinhLuanDanhGia> model = db.BinhLuanDanhGias;
            if (!string.IsNullOrEmpty(searchString))
            {
                model = model.Where(x => x.KhachHang.tenKH.Contains(searchString) || x.maKH.Contains(searchString) || x.SanPham.tenSP.Contains(searchString) || x.maSP.Contains(searchString) || x.soSaoDanhGia.Contains(searchString) || x.noiDungBinhLuan.Contains(searchString));
            }
            return model.ToList();
        }
        //Lấy ra 1 luận đánh giá
        public BinhLuanDanhGia InfoBinhLuanDanhGia(string makh, string masp)
        {
            BinhLuanDanhGia bldg = db.BinhLuanDanhGias.Where(x => x.maKH.Contains(makh) && x.maSP.Contains(masp)).FirstOrDefault();
            return bldg;
        }
        //Cập nhật luận đánh giá
        public bool UpdateBinhLuanDanhGia(BinhLuanDanhGia bldg)
        {
            try
            {
                BinhLuanDanhGia dg = InfoBinhLuanDanhGia(bldg.maKH, bldg.maSP);
                dg.soSaoDanhGia = bldg.soSaoDanhGia;
                dg.noiDungBinhLuan = bldg.noiDungBinhLuan;
                db.SaveChanges();
                return true;
            }
            catch (Exception) { }
            return false;
        }

        #endregion

        #region Hàm khác

        //Tính số lượng của sản phẩm có trong thông tin sản phẩm Controller Sanpham => ListSanPhamAutoUpdate
        public int TinhSoLuong(string masanpham)
        {
            int tb = 0;
            try
            {
                var sosao = db.ThongTinSanPhams.Where(x => x.maSP.Contains(masanpham) && x.tinhTrang.Contains("Còn trong kho"));
                tb = sosao.Count();
                var model = db.SanPhams.Find(masanpham);
                model.soLuong = tb;
                db.SaveChanges();
            }
            catch (Exception) { }
            return tb;
        }

        //Tính tiền tất cả giá bán trong thông tin sản phẩm
        public bool TinhTienGiaBan(string maphieunhap)
        {
            try
            {
                var sosao = db.ThongTinSanPhams;
                foreach (var item in sosao)
                {
                    item.giaBan = item.giaGoc + (item.giaGoc * 10 / 100);
                }
                db.SaveChanges();
                return true;
            }
            catch (Exception) { }
            return false;
        }

        #endregion

    }
}
