﻿/*
 * by nguyen dinh tuan
 */
//hien thong bao loi Alert
$(function () {
    $('#AlertBox').removeClass('hide');
    $('#AlertBox').delay(10000).slideUp(1000);
});

//thông báo toastr
$(function () {
    if ($('#success').val()) {
        displayMessage($('#success').val(), 'success');
    }
    if ($('#info').val()) {
        displayMessage($('#info').val(), 'info');
    }
    if ($('#warning').val()) {
        displayMessage($('#warning').val(), 'warning');
    }
    if ($('#error').val()) {
        displayMessage($('#error').val(), 'error');
    }
});
var displayMessage = function (message, msgType) {
    toastr.options = {
        "closeButton": false,
        "debug": false,
        "newestOnTop": true,
        "progressBar": true,
        "positionClass": "toast-top-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "30000",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "show",
        "hideMethod": "hide"
    };
    toastr[msgType](message);
};

//hủy đơn hàng
function functionhuydonhang(madh) {
    bootbox.dialog({
        title: "Lý do hủy đơn hàng: " + madh,
        message: '<div class="row">' +
            '<div class="col-md-12">' +
            '<div class="panel-body">' +
            '<div class="form-group">' +
            '<div class="col-sm-12">' +
            '<input type="text" name="lydo" placeholder="Nhập lý do" class="form-control" form="' + madh + '" required />' +
            '</div>' +
            '</div>' +
            '</div>' +
            '<div class="panel-footer">' +
            '<div class="row">' +
            '<div class="col-sm-9 col-sm-offset-3 text-right">' +
            '<button class="btn btn-success btn-labeled fa fa-check fa-lg text-right m-r-10" form="' + madh + '" type="submit">Lưu</button>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>',
    });
};

//in báo cáo
document.getElementById("doPrint").addEventListener("click", function () {
    var printContents = document.getElementById('printDiv').innerHTML;
    var originalContents = document.body.innerHTML;
    document.body.innerHTML = printContents;
    window.print();
    document.body.innerHTML = originalContents;
});
