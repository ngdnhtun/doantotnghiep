﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BanXeMoTo.Models
{
    public class LoginModel
    {
        public string TaiKhoan { get; set; }
        public string MaKH { get; set; }
        public string MatKhau { get; set; }
        public string TenKH { get; set; }
        public string GioiTinh { get; set; }
        public decimal? TienThuong { get; set; }
    }
}