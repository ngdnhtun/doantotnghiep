﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BanXeMoTo.Common
{
    public class Constants
    {
        public const string ADMIN_SESSION = "ADMIN_SESSION";
        public const string USER_SESSION = "USER_SESSION";
        public const string CART_SESSION = "CART_SESSION";
    }
}