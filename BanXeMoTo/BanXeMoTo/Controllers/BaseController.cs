﻿using BanXeMoTo.Common;
using BanXeMoTo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace BanXeMoTo.Controllers
{
    public class BaseController : Controller
    {
        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            LoginModel session = (LoginModel)Session[Constants.USER_SESSION];
            if (session == null)
            {
                filterContext.Result = new RedirectToRouteResult(new
                    RouteValueDictionary(new { Controller = "Login", action = "Index" }));
            }
            base.OnActionExecuted(filterContext);
        }

        protected void SetAlert(string message, string type)
        {
            TempData["AlertMessage"] = message;
            if (type == "success")
            {
                TempData["AlertType"] = "alert-success";
            }
            else if (type == "warning")
            {
                TempData["AlertType"] = "alert-warning";
            }
            else if (type == "error")
            {
                TempData["AlertType"] = "alert-danger";
            }
        }

        protected void ThongBao(string message, string type)
        {
            if (type == "success")
            {
                TempData["success"] = message;
            }
            else if (type == "info")
            {
                TempData["info"] = message;
            }
            else if (type == "warning")
            {
                TempData["warning"] = message;
            }
            else if (type == "error")
            {
                TempData["error"] = message;
            }
        }
    }
}