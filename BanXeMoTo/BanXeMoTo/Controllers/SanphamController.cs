﻿using BanXeMoTo.Common;
using BanXeMoTo.Models;
using Models.DAO;
using Models.EF;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BanXeMoTo.Controllers
{
    public class SanphamController : TbController
    {
        private UserDao dao = new UserDao();

        // GET: Sanpham
        public ActionResult Index(int page = 1, int pagesize = 15)
        {
            var model = dao.ListSanPhamAutoUpdate();
            return View(model.ToPagedList(page, pagesize));
        }

        public ActionResult Search(string searchString, int page = 1, int pagesize = 15)
        {
            if (string.IsNullOrEmpty(searchString))
            {
                ThongBao("Hãy nhập tên sản phẩm", "warning");
                return RedirectToAction("Index");
            }
            else
            {
                var model = dao.ListWhereSanPhamAll(searchString);
                if (model.Count == 0)
                {
                    ThongBao("Không tìm thấy sản phẩm \"" + searchString + "\"", "warning");
                    return RedirectToAction("Index");
                }
                ViewBag.SearchString = searchString;
                return View(model.ToPagedList(page, pagesize));
            }
        }

        public ActionResult Details(string masanpham, string mausacchon)
        {
            SanPham model = dao.InfoSanPham(masanpham);
            if (model == null || string.IsNullOrEmpty(masanpham))
            {
                return RedirectToAction("Index");
            }
            dao.TangLuotXem(masanpham);
            ViewBag.SoLuong = dao.TinhSoLuong(masanpham);
            ViewBag.DanhGia = dao.TinhTiLeDanhGia(masanpham);
            ViewBag.MauSac = dao.LayMauSac(masanpham);

            ViewBag.XemNhieu = dao.ListXemNhieu();
            ViewBag.MuaNhieu = dao.ListMuaNhieu();

            ViewBag.BinhLuanDanhGia = dao.BinhLuanDanhGia(masanpham);
            ViewBag.CountBLDG = dao.BinhLuanDanhGia(masanpham).Count();

            if (!string.IsNullOrEmpty(mausacchon))
            {
                ViewBag.MauSacChon = mausacchon;
                decimal? giamausacchon = dao.LayGiaThongTinSanPham(mausacchon).giaBan;
                model.giaBan = String.Format("{0:0,0 VNĐ}", giamausacchon);
            }

            var session = (LoginModel)Session[Constants.USER_SESSION];
            if (session != null)
            {
                ViewBag.KTMH = dao.KiemTraMuaHang(session.MaKH, masanpham);
            }
            else
            {
                ViewBag.KTMH = 2;
            }
            return View(model);
        }

        public ActionResult Danhgia(string masanpham, string sosaodanhgia, string noidungbinhluan)
        {
            LoginModel session = (LoginModel)Session[Constants.USER_SESSION];
            if (session != null)
            {
                var danhgia = new BinhLuanDanhGia();
                danhgia.maKH = session.MaKH;
                danhgia.maSP = masanpham;
                danhgia.soSaoDanhGia = sosaodanhgia;
                danhgia.noiDungBinhLuan = noidungbinhluan;
                danhgia.ngayDanhGia = DateTime.Now;

                var dg = dao.TaiBinhLuanDanhGia(danhgia);
                if (dg == 1)
                {
                    ThongBao("Đã đăng tải bình luận thành công", "success");
                }
                else if (dg == 2)
                {
                    ThongBao("Cập nhật nhận xét thành công", "success");
                }
                else
                {
                    ThongBao("Không thể đánh giá", "error");
                }
            }
            return RedirectToAction("Details", "SanPham", new { masanpham });
        }

        public ActionResult Honda(string loaisp, int page = 1, int pagesize = 15)
        {
            if (!string.IsNullOrEmpty(loaisp))
            {
                var model1 = dao.ListLoaiSanPhamAll(loaisp);
                ViewBag.Category = model1.FirstOrDefault().LoaiSanPham.tenLSP;

                return View(model1.ToPagedList(page, pagesize));
            }

            var model = dao.ListSanPhamHSX("HD");
            return View(model.ToPagedList(page, pagesize));
        }

        public ActionResult Yamaha(string loaisp, int page = 1, int pagesize = 15)
        {
            if (!string.IsNullOrEmpty(loaisp))
            {
                var model1 = dao.ListLoaiSanPhamAll(loaisp);
                ViewBag.Category = model1.FirstOrDefault().LoaiSanPham.tenLSP;

                return View(model1.ToPagedList(page, pagesize));
            }

            var model = dao.ListSanPhamHSX("YA");
            return View(model.ToPagedList(page, pagesize));
        }

        public ActionResult Kawasaki(string loaisp, int page = 1, int pagesize = 15)
        {
            if (!string.IsNullOrEmpty(loaisp))
            {
                var model1 = dao.ListLoaiSanPhamAll(loaisp);
                ViewBag.Category = model1.FirstOrDefault().LoaiSanPham.tenLSP;

                return View(model1.ToPagedList(page, pagesize));
            }

            var model = dao.ListSanPhamHSX("KA");
            return View(model.ToPagedList(page, pagesize));
        }

    }
}