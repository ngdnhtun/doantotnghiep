﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BanXeMoTo.Controllers
{
    public class TbController : Controller
    {
        protected void ThongBao(string message, string type)
        {
            if (type == "success")
            {
                TempData["success"] = message;
            }
            else if (type == "info")
            {
                TempData["info"] = message;
            }
            else if (type == "warning")
            {
                TempData["warning"] = message;
            }
            else if (type == "error")
            {
                TempData["error"] = message;
            }
        }
    }
}