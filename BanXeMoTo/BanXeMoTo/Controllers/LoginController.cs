﻿using BanXeMoTo.Common;
using BanXeMoTo.Models;
using Models.DAO;
using Models.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BanXeMoTo.Controllers
{
    public class LoginController : TbController
    {
        private UserDao dao = new UserDao();

        // GET: Login
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(LoginModel user)
        {
            if (ModelState.IsValid)
            {
                int result = dao.Login(user.TaiKhoan, Encryptor.EncryptMD5(user.MatKhau));
                //(user.MatKhau)
                if (result == 1)
                {
                    //ModelState.AddModelError("", "Đăng nhập thành công!");
                    KhachHang info = dao.InfoKhachHang(user.TaiKhoan);
                    if (!string.IsNullOrEmpty(info.trangThai))
                    {
                        TempData["AlertType"] = "alert-danger";
                        TempData["AlertMessage"] = "Tài khoản của bạn đã bị khóa!";
                        return RedirectToAction("Index", "Login");
                    }
                    user.MaKH = info.maKH;
                    user.TenKH = info.tenKH;
                    user.GioiTinh = info.gioiTinh;
                    user.TienThuong = info.tienThuong;
                    Session.Add(Constants.USER_SESSION, user);

                    //có - không
                    if (dao.InfoGioHang(info.maKH) && Session[Constants.CART_SESSION] == null)
                    {
                        Session[Constants.CART_SESSION] = new List<CartModel>();
                        List<CartModel> cart = Session[Constants.CART_SESSION] as List<CartModel>;

                        List<GioHang> giohang = dao.ListGioHang(info.maKH);
                        foreach (var gh in giohang)
                        {
                            CartModel item = new CartModel()
                            {
                                MaSP = gh.maSP,
                                TenSP = gh.tenSP,
                                MauSac = gh.mauSac,
                                GiaBan = gh.giaBan,
                                AnhSP = gh.anhSP
                            };
                            cart.Insert(0, item);
                        }
                    }
                    else
                    {
                        //không - có
                        if (!dao.InfoGioHang(info.maKH) && Session[Constants.CART_SESSION] != null)
                        {
                            List<CartModel> cart = Session[Constants.CART_SESSION] as List<CartModel>;
                            foreach (var item in cart)
                            {
                                GioHang gh = new GioHang()
                                {
                                    maGH = dao.TaoMaGioHang(),
                                    maKH = info.maKH,
                                    maSP = item.MaSP,
                                    tenSP = item.TenSP,
                                    mauSac = item.MauSac,
                                    giaBan = item.GiaBan,
                                    anhSP = item.AnhSP
                                };
                                dao.AddGioHangVaoTable(gh);
                            }
                        }
                        else
                        {
                            //có - có
                            if (dao.InfoGioHang(info.maKH) && Session[Constants.CART_SESSION] != null)
                            {
                                List<CartModel> cart = Session[Constants.CART_SESSION] as List<CartModel>;
                                foreach (var item in cart)
                                {
                                    if (dao.CheckGioHang(info.maKH, item.MaSP, item.MauSac))
                                    {
                                        GioHang gh = new GioHang()
                                        {
                                            maGH = dao.TaoMaGioHang(),
                                            maKH = info.maKH,
                                            maSP = item.MaSP,
                                            tenSP = item.TenSP,
                                            mauSac = item.MauSac,
                                            giaBan = item.GiaBan,
                                            anhSP = item.AnhSP
                                        };
                                        dao.AddGioHangVaoTable(gh);
                                    }
                                }
                                cart.Clear();
                                List<GioHang> giohang = dao.ListGioHang(info.maKH);
                                foreach (var gh in giohang)
                                {
                                    CartModel item = new CartModel()
                                    {
                                        MaSP = gh.maSP,
                                        TenSP = gh.tenSP,
                                        MauSac = gh.mauSac,
                                        GiaBan = gh.giaBan,
                                        AnhSP = gh.anhSP
                                    };
                                    cart.Insert(0, item);
                                }
                            }
                        }
                    }

                    ThongBao("Đăng nhập thành công", "success");
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    //ModelState.AddModelError("", "Tên đăng nhập hoặc mật khẩu sai!");
                    TempData["AlertType"] = "alert-danger";
                    TempData["AlertMessage"] = "Sai thông tin đăng nhập!";

                    ThongBao("Đăng nhập thất bại", "error");
                }
            }
            return View("Index");
        }
    }
}