﻿using BanXeMoTo.Common;
using Models.DAO;
using Models.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BanXeMoTo.Controllers
{
    public class SignupController : TbController
    {
        private UserDao dao = new UserDao();

        // GET: Signup
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(KhachHang model, int year, int month, int day)
        {
            if (ModelState.IsValid)
            {
                string makhachhang = dao.TaoMaKhachHang();

                model.maKH = makhachhang;
                model.nickName = model.matKhau;
                model.matKhau = Encryptor.EncryptMD5(model.matKhau);
                DateTime ngaysinh = new DateTime(year, month, day);
                model.ngaySinh = ngaysinh;
                model.tienThuong = 0;

                KhachHang ktsdt = dao.InfoKhachHang(model.SDT);
                KhachHang ktemail = dao.InfoKhachHang(model.Email);
                if (ktsdt != null || ktemail != null)
                {
                    TempData["AlertType"] = "alert-danger";
                    TempData["AlertMessage"] = "Số điện thoại hoặc Email này đã được đăng ký!";

                    ThongBao("Đăng ký thất bại", "error");
                    ThongBao("Hãy kiểm tra lại Số điện thoại hoặc Email", "warning");
                }
                else
                {
                    bool result = dao.Insert(model);
                    if (result)
                    {
                        TempData["AlertType"] = "alert-success";
                        TempData["AlertMessage"] = "Đăng ký thành công! Giờ bạn có thể đăng nhập";

                        ThongBao("Đăng ký thành công", "success");
                        return RedirectToAction("Index", "Login");
                    }
                    else
                    {
                        TempData["AlertType"] = "alert-danger";
                        TempData["AlertMessage"] = "Đăng ký thất bại! Hãy kiểm tra lại thông tin";

                        ThongBao("Đăng ký thất bại", "error");
                        ThongBao("Hãy kiểm tra lại thông tin", "warning");
                    }
                }
            }
            return View("Index");
        }
    }
}