﻿using BanXeMoTo.Common;
using BanXeMoTo.Models;
using Models.DAO;
using Models.EF;
using System.Collections.Generic;
using System.Web.Mvc;

namespace BanXeMoTo.Controllers
{
    public class HomeController : TbController
    {
        private UserDao dao = new UserDao();

        public ActionResult Index()
        {
            ViewBag.NgauNhien = dao.ListNgauNhien();
            ViewBag.DanhGiaCao = dao.ListDanhGiaCao();
            ViewBag.MuaNhieu = dao.ListMuaNhieu();
            ViewBag.XemNhieu = dao.ListXemNhieu();
            ViewBag.BaDanhGiaCao = dao.BaDanhGiaCao();
            ViewBag.BaDanhGiaThap = dao.BaDanhGiaThap();

            return View();
        }

        public ActionResult Logout()
        {
            Session[Constants.USER_SESSION] = null;
            Session[Constants.CART_SESSION] = null;
            ThongBao("Bạn đã đăng xuất", "info");
            return RedirectToAction("Index");
        }

        [ChildActionOnly]
        public ActionResult MenuNavPartial()
        {
            var model = dao.ListLoaiSanPham();
            return PartialView(model);
        }

        [ChildActionOnly]
        public ActionResult MenuLeftPartial()
        {
            var model = dao.ListLoaiSanPham();
            return PartialView(model);
        }

        public void CartPartial()
        {
            LoginModel session = (LoginModel)Session[Constants.USER_SESSION];
            if (session != null)
            {
                KhachHang info = dao.InfoKhachHang(session.MaKH);
                //có - có
                if (Session[Constants.CART_SESSION] == null)
                {
                    Session[Constants.CART_SESSION] = new List<CartModel>();
                }
                List<CartModel> cart = Session[Constants.CART_SESSION] as List<CartModel>;
                cart.Clear();
                List<GioHang> giohang = dao.ListGioHang(info.maKH);
                foreach (var gh in giohang)
                {
                    CartModel item = new CartModel()
                    {
                        MaSP = gh.maSP,
                        TenSP = gh.tenSP,
                        MauSac = gh.mauSac,
                        GiaBan = gh.giaBan,
                        AnhSP = gh.anhSP
                    };
                    cart.Insert(0, item);
                }
            }
        }
    }
}