﻿using BanXeMoTo.Common;
using BanXeMoTo.Models;
using Models.DAO;
using Models.EF;
using PagedList;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BanXeMoTo.Controllers
{
    public class TaikhoanController : BaseController
    {
        private UserDao dao = new UserDao();

        // GET: Taikhoan
        public ActionResult Index()
        {
            LoginModel session = (LoginModel)Session[Constants.USER_SESSION];
            if (session != null)
            {
                var model = dao.InfoKhachHang(session.MaKH);
                return View(model);
            }
            return View();
        }

        public ActionResult Doimatkhau()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Doimatkhau(string matkhaucu, string matkhaumoi, string matkhaumoi2)
        {
            LoginModel session = (LoginModel)Session[Constants.USER_SESSION];
            if (ModelState.IsValid)
            {
                if (session != null)
                {
                    if (session.MatKhau != matkhaucu)
                    {
                        ThongBao("Mật khẩu cũ không chính xác", "warning");
                    }
                    else if (matkhaumoi != matkhaumoi2)
                    {
                        ThongBao("Mật khẩu mới nhập lại không chính xác", "warning");
                    }
                    else
                    {
                        var model = dao.DoiMatKhau(session.MaKH, Encryptor.EncryptMD5(matkhaumoi), matkhaumoi);
                        if (model)
                        {
                            session.MatKhau = matkhaumoi;
                            ThongBao("Đổi mật khẩu thành công", "success");
                            return RedirectToAction("Index");
                        }
                        else
                        {
                            ThongBao("Đổi mật khẩu thất bại", "error");
                        }
                    }
                }
            }
            return View();
        }

        public ActionResult Suathongtin()
        {
            LoginModel session = (LoginModel)Session[Constants.USER_SESSION];
            if (session != null)
            {
                var model = dao.InfoKhachHang(session.MaKH);
                return View(model);
            }
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Suathongtin(KhachHang model, int day, int month, int year)
        {
            LoginModel session = (LoginModel)Session[Constants.USER_SESSION];
            if (ModelState.IsValid)
            {
                if (session != null)
                {
                    model.maKH = session.MaKH;
                    DateTime ngaysinh = new DateTime(year, month, day);
                    model.ngaySinh = ngaysinh;
                    var result = dao.CapNhatThongTin(model);
                    if (result)
                    {
                        ThongBao("Cập nhật thông tin thành công", "success");
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        ThongBao("Cập nhật thông tin thất bại", "error");
                    }
                }
            }
            return View();
        }

        public ActionResult Donhang(int page = 1, int pagesize = 5)
        {
            LoginModel session = (LoginModel)Session[Constants.USER_SESSION];
            if (session != null)
            {
                var donhang = dao.ListDonHang(session.MaKH);
                var daynow = DateTime.Now.AddDays(-1);
                if (donhang.Count != 0)
                {
                    ViewBag.DonHang = 1;
                    ViewBag.Huy = daynow;
                }
                else
                {
                    ViewBag.DonHang = 0;
                }
                return View(donhang.ToPagedList(page, pagesize));
            }
            return View();
        }

        [HttpPost]
        public ActionResult Donhang(string madonhang)
        {
            LoginModel session = (LoginModel)Session[Constants.USER_SESSION];
            if (session != null)
            {
                var donhang = dao.HuyDonHang(madonhang);
                if (donhang)
                {
                    ThongBao("Đã hủy đơn hàng thành công", "success");
                }
                else
                {
                    ThongBao("Không thể hủy đơn hàng", "success");
                }
            }
            return RedirectToAction("DonHang");
        }

        public ActionResult Xemdonhang(string madonhang)
        {
            LoginModel session = (LoginModel)Session[Constants.USER_SESSION];
            if (session != null)
            {
                ArrayList dh = new ArrayList();
                var chitietdonhang = dao.ListChiTietDonHang(madonhang);
                var donhang = dao.InfoDonHang(madonhang);
                var daynow = DateTime.Now.AddDays(-1);
                foreach (var item in chitietdonhang)
                {
                    var sanpham = dao.InfoSanPham(item.maSP);
                    CartModel ca = new CartModel()
                    {
                        MaSP = sanpham.maSP,
                        TenSP = sanpham.tenSP,
                        AnhSP = sanpham.anhSP,
                        GiaBan = item.thanhTien,
                        MauSac = item.mauSac
                    };
                    dh.Add(ca);
                }
                ViewBag.Huy = daynow;
                ViewBag.ChiTietDonHang = dh;
                return View(donhang);
            }
            return View();
        }

        public ActionResult Lichsu(int page = 1, int pagesize = 5)
        {
            LoginModel session = (LoginModel)Session[Constants.USER_SESSION];
            if (session != null)
            {
                var donhang = dao.ListDonHangAll(session.MaKH);
                if (donhang.Count != 0)
                {
                    ViewBag.DonHang = 1;
                }
                else
                {
                    ViewBag.DonHang = 0;
                }
                return View(donhang.ToPagedList(page, pagesize));
            }
            return View();
        }

        public ActionResult Danhgia()
        {
            return View();
        }
    }
}