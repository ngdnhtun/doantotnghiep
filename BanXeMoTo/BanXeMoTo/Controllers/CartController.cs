﻿using BanXeMoTo.Common;
using BanXeMoTo.Models;
using Models.DAO;
using Models.EF;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity.Migrations.History;
using System.Data.Entity.Migrations.Model;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebGrease.Css.Extensions;

namespace BanXeMoTo.Controllers
{
    public class CartController : TbController
    {
        private UserDao dao = new UserDao();

        // GET: Cart
        public ActionResult Index()
        {
            if (Session[Constants.CART_SESSION] != null)
            {
                List<CartModel> cart = Session[Constants.CART_SESSION] as List<CartModel>;
                foreach (var item in cart)
                {
                    //lấy màu sắc từng mã sản phẩm
                    ViewData[item.MaSP] = dao.LayMauSac(item.MaSP);
                }
            }
            ViewBag.NgauNhien = dao.ListNgauNhien().Take(4);

            return View();
        }

        [HttpPost]
        public ActionResult Addcart(string masanpham, string mausacchon)
        {
            if (Session[Constants.CART_SESSION] == null)
            {
                Session[Constants.CART_SESSION] = new List<CartModel>();
            }

            List<CartModel> cart = Session[Constants.CART_SESSION] as List<CartModel>;
            SanPham result = dao.InfoSanPham(masanpham);
            ArrayList mau = dao.LayMauSac(masanpham);

            int ktmau = dao.TinhMau(masanpham, mausacchon);
            //kiểm tra màu đã chọn trong danh sách giỏ hàng có nhiều hơn ko
            var listmaumoi = cart.Where(x => x.MaSP.Equals(masanpham) && x.MauSac.Equals(mausacchon));
            int demmaumoi = listmaumoi.Count();

            if (string.IsNullOrEmpty(mausacchon))
            {
                ThongBao("Hãy chọn màu sắc sản phẩm mà bạn muốn", "info");
            }
            else if (mau.Count == 0 || demmaumoi >= ktmau)
            {
                ThongBao("Sản phẩm tạm hết không thể thêm vào giỏ", "warning");
            }
            else if (cart.Count(x => x.MaSP.Contains(masanpham)) == dao.TinhSoLuong(masanpham))
            {
                ThongBao("Sản phẩm chỉ được thêm giới hạn", "warning");
            }
            else
            {
                ThongTinSanPham ttsp = dao.LayGiaThongTinSanPham(mausacchon);
                CartModel itemcart = new CartModel()
                {
                    MaSP = masanpham,
                    TenSP = result.tenSP,
                    MauSac = ttsp.mauSac,
                    GiaBan = ttsp.giaBan,
                    AnhSP = result.anhSP
                };
                cart.Insert(0, itemcart);
                LoginModel session = (LoginModel)Session[Constants.USER_SESSION];
                if (session != null)
                {
                    GioHang gh = new GioHang()
                    {
                        maGH = dao.TaoMaGioHang(),
                        maKH = session.MaKH,
                        maSP = masanpham,
                        tenSP = result.tenSP,
                        mauSac = ttsp.mauSac,
                        giaBan = ttsp.giaBan,
                        anhSP = result.anhSP
                    };
                    dao.AddGioHangVaoTable(gh);
                }

                ThongBao("Đã thêm sản phẩm vào giỏ hàng", "success");
            }
            return RedirectToAction("Details", "SanPham", new { masanpham });
        }

        public void Delete(string masanpham, string mausac, string url)
        {
            if (Session[Constants.CART_SESSION] != null)
            {
                List<CartModel> cart = Session[Constants.CART_SESSION] as List<CartModel>;
                CartModel itemXoa = cart.FirstOrDefault(x => x.MaSP.Equals(masanpham) && x.MauSac.Equals(mausac));
                if (itemXoa != null)
                {
                    ThongBao("Đâ xóa sản phẩm khỏi giỏ hàng", "success");
                    cart.Remove(itemXoa);
                    LoginModel session = (LoginModel)Session[Constants.USER_SESSION];
                    if (session != null)
                    {
                        dao.XoaGioHang(session.MaKH, masanpham, mausac);
                    }
                }
                if (cart.Count() == 0)
                {
                    ThongBao("Giỏ hàng hiện không có sản phẩm nào", "info");
                    Session[Constants.CART_SESSION] = null;
                }
            }
            Response.Redirect(url);
        }

        public ActionResult Clear()
        {
            ThongBao("Giỏ hàng đã xóa", "info");
            Session[Constants.CART_SESSION] = null;
            LoginModel session = (LoginModel)Session[Constants.USER_SESSION];
            if (session != null)
            {
                dao.XoaGioHangALL(session.MaKH);
            }

            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult Update(string masanpham, string chonmau, string maucu)
        {
            if (Session[Constants.CART_SESSION] != null)
            {
                List<CartModel> cart = Session[Constants.CART_SESSION] as List<CartModel>;
                ArrayList mau = dao.LayMauSac(masanpham);
                if (chonmau != "0")
                {
                    //kiểm tra màu sản phẩm đã chọn còn bao nhiêu
                    int ktmau = dao.TinhMau(masanpham, chonmau);

                    //kiểm tra màu đã chọn trong danh sách giỏ hàng có nhiều hơn ko
                    var listmaumoi = cart.Where(x => x.MaSP.Equals(masanpham) && x.MauSac.Equals(chonmau));
                    int demmaumoi = listmaumoi.Count();

                    var listmaucu = cart.Where(x => x.MaSP.Equals(masanpham) && x.MauSac.Equals(maucu));
                    if (demmaumoi < ktmau)
                    {
                        foreach (var item in listmaucu)
                        {
                            item.GiaBan = dao.LayGiaBan(item.MaSP, chonmau);
                            LoginModel session = (LoginModel)Session[Constants.USER_SESSION];
                            if (session != null)
                            {
                                dao.UpdateGioHang(session.MaKH, item.MaSP, item.MauSac, chonmau, item.GiaBan);
                            }
                            item.MauSac = chonmau;

                            break;
                        }
                        ThongBao("Cập nhật màu thành công", "success");
                    }
                    else
                    {
                        ThongBao("Sản phẩm có màu này hiện giới hạn", "warning");
                    }
                }
                else
                {
                    ThongBao("Bạn chưa thay đổi màu", "info");
                }
            }
            return RedirectToAction("Index");
        }

        public ActionResult Thanhtoan(string chon)
        {
            LoginModel session = (LoginModel)Session[Constants.USER_SESSION];

            if (chon == "ok")
            {
                ThongBao("Bạn đã đặt hàng thành công", "success");
                ViewBag.Chon = chon;
            }
            else if (session == null)
            {
                ThongBao("Bạn phải đăng nhập mới có thể mua", "error");
                return RedirectToAction("Index");
            }
            else if (Session[Constants.CART_SESSION] == null)
            {

                ThongBao("Hãy chọn một sản phẩm trước", "warning");
                return RedirectToAction("Index");
            }
            else
            {
                List<CartModel> cart = Session[Constants.CART_SESSION] as List<CartModel>;
                int a = cart.Where(x => x.MauSac == "").Count();

                if (a != 0)
                {
                    ThongBao("Có sản phẩm bạn chưa chọn màu", "error");
                    return RedirectToAction("Index");
                }
                else
                {
                    ViewBag.Buoc1 = "active";
                    ViewBag.Buoc2 = "";
                    ViewBag.Buoc3 = "";
                    ViewBag.Buoc11 = "block";
                    ViewBag.Buoc22 = "none";
                    ViewBag.Buoc33 = "none";
                }
            }
            ViewBag.ThongTinNguoiMua = dao.InfoKhachHang(session.MaKH);
            ViewBag.DiaChiNhan = ViewBag.ThongTinNguoiMua.diaChiKH;

            return View();
        }

        [HttpPost]
        public ActionResult Thanhtoan(string diachinhanhang, string luudiachi, string buoc)
        {
            LoginModel session = (LoginModel)Session[Constants.USER_SESSION];
            ChiTietDonHang ctdh = new ChiTietDonHang();
            DonHang donhang = new DonHang();

            if (session == null)
            {
                ThongBao("Bạn phải đăng nhập mới có thể mua", "error");
                return RedirectToAction("Index");
            }
            else if (Session[Constants.CART_SESSION] == null)
            {
                ThongBao("Hãy chọn một sản phẩm trước", "warning");
                return RedirectToAction("Index");
            }
            else
            {
                List<CartModel> cart = Session[Constants.CART_SESSION] as List<CartModel>;
                int a = cart.Where(x => x.MauSac == "").Count();
                if (a != 0)
                {
                    ThongBao("Có sản phẩm bạn chưa chọn màu", "error");
                    return RedirectToAction("Index");
                }
                else if (buoc == "1")
                {
                    ViewBag.Buoc1 = "active";
                    ViewBag.Buoc2 = "active";
                    ViewBag.Buoc3 = "";
                    ViewBag.Buoc11 = "none";
                    ViewBag.Buoc22 = "block";
                    ViewBag.Buoc33 = "none";

                    if (luudiachi == "1")
                    {
                        dao.LuuDiaChi(session.MaKH, diachinhanhang);
                        ThongBao("Địa chỉ đã được lưu", "success");
                    }
                }
                else if (buoc == "2")
                {
                    ViewBag.Buoc1 = "active";
                    ViewBag.Buoc2 = "active";
                    ViewBag.Buoc3 = "active";
                    ViewBag.Buoc11 = "none";
                    ViewBag.Buoc22 = "none";
                    ViewBag.Buoc33 = "block";

                    ThongBao("Mọi thứ đã sẵn sàng", "success");
                }
                else if (buoc == "3")
                {
                    decimal? tongphu = 0, tongtien = 0, tonggiam = 0, gia = 0;
                    int tsl = 0, km = 0;
                    tsl = cart.Where(x => x.MauSac != "").Count();
                    gia = cart.Where(x => x.MauSac != "").Sum(x => x.GiaBan);
                    km += tsl;
                    tonggiam = gia * km / 100;
                    tongphu = gia;
                    tongtien = gia - tonggiam;

                    donhang.maDH = dao.TaoMaDonHang();
                    donhang.maKH = session.MaKH;
                    donhang.ngayDatHang = DateTime.Now;
                    donhang.ngayGiaoHang = DateTime.Now.AddDays(7);
                    donhang.diaChiGiaoHang = diachinhanhang;
                    donhang.tongSoLuong = tsl;
                    donhang.tongPhu = tongphu;
                    donhang.tongGiam = tonggiam;
                    donhang.tongTien = tongtien;
                    donhang.trangThai = "Chờ xử lý";

                    bool taodonhang = dao.TaoDonHang(donhang);
                    if (taodonhang)
                    {
                        foreach (var item in cart)
                        {
                            ctdh.maDH = donhang.maDH;
                            ctdh.maSP = item.MaSP;
                            ctdh.mauSac = item.MauSac;
                            ctdh.thanhTien = item.GiaBan;

                            dao.TaoChiTietDonHang(ctdh);
                            dao.TangLuotMua(item.MaSP);
                            dao.UpdateTinhTrangThongTinSanPham(item.MaSP, item.MauSac, donhang.maDH);
                        }
                        Session[Constants.CART_SESSION] = null;
                        dao.XoaGioHangALL(session.MaKH);
                        ThongBao("Bạn đã đặt hàng thành công", "success");
                        return RedirectToAction("Thanhtoan", new { chon = "ok" });
                    }
                    else
                    {
                        ThongBao("Đặt hàng không thành công", "error");
                    }
                }
            }
            ViewBag.ThongTinNguoiMua = dao.InfoKhachHang(session.MaKH);
            ViewBag.DiaChiNhan = diachinhanhang;

            return View();
        }
    }
}