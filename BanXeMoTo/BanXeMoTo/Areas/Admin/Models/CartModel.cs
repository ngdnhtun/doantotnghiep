﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BanXeMoTo.Areas.Admin.Models
{
    public class CartModel
    {
        public string MaSP { get; set; }
        public string TenSP { get; set; }
        public string MauSac { get; set; }
        public decimal? GiaBan { get; set; }
        public string AnhSP { get; set; }
    }
}