﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BanXeMoTo.Areas.Admin.Models
{
    public class LoginModel
    {
        public string TaiKhoan { get; set; }
        public string MaNV { get; set; }
        public string MatKhau { get; set; }
        public string TenNV { get; set; }
        public string TenCV { get; set; }
        public string Quyen { get; set; }
        public string GioiTinh { get; set; }
    }
}