﻿using BanXeMoTo.Areas.Admin.Models;
using BanXeMoTo.Common;
using Models.DAO;
using Models.EF;
using PagedList;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BanXeMoTo.Areas.Admin.Controllers
{
    public class OrderController : BaseController
    {
        private AdminDao dao = new AdminDao();

        // GET: Admin/Order
        public ActionResult Index(string searchString, int page = 1, int pagesize = 10)
        {
            if (QuyenNVBH())
            {
                return RedirectToAction("Index", "Home");
            }
            List<DonHang> model;
            if (string.IsNullOrEmpty(searchString))
            {
                model = dao.ListDonHangDangDat();
            }
            else
            {
                model = dao.ListDonHangDangDat(searchString);
                if (model.Count == 0)
                {
                    SetAlert("Không tìm thấy đơn hàng " + searchString, "warning");
                }
            }
            ViewBag.Tong = model.Count();
            ViewBag.SoLuong = dao.ListDonHangDangDat().Count();
            ViewBag.searchString = searchString;
            return View(model.ToPagedList(page, pagesize));
        }

        public ActionResult Shipping(string searchString, int page = 1, int pagesize = 10)
        {
            if (QuyenNVBH())
            {
                return RedirectToAction("Index", "Home");
            }
            List<DonHang> model;
            if (string.IsNullOrEmpty(searchString))
            {
                model = dao.ListDonHangDangVanChuyen();
            }
            else
            {
                model = dao.ListDonHangDangVanChuyen(searchString);
                if (model.Count == 0)
                {
                    SetAlert("Không tìm thấy đơn hàng " + searchString, "warning");
                }
            }
            ViewBag.Tong = model.Count();
            ViewBag.SoLuong = dao.ListDonHangDangVanChuyen().Count();
            ViewBag.searchString = searchString;
            return View(model.ToPagedList(page, pagesize));
        }

        public ActionResult Allorder(string searchString, int page = 1, int pagesize = 10)
        {
            if (QuyenNVBH())
            {
                return RedirectToAction("Index", "Home");
            }
            List<DonHang> model;
            if (string.IsNullOrEmpty(searchString))
            {
                model = dao.ListDonHang();
            }
            else
            {
                model = dao.ListDonHang(searchString);
                if (model.Count == 0)
                {
                    SetAlert("Không tìm thấy đơn hàng " + searchString, "warning");
                }
            }
            ViewBag.Tong = model.Count();
            ViewBag.SoLuong = dao.ListDonHang().Count();
            ViewBag.searchString = searchString;
            return View(model.ToPagedList(page, pagesize));
        }

        public ActionResult Donnhan(string searchString, int page = 1, int pagesize = 10)
        {
            if (QuyenNVBH())
            {
                return RedirectToAction("Index", "Home");
            }
            List<DonHang> model;
            if (string.IsNullOrEmpty(searchString))
            {
                model = dao.ListDonHangDaNhan();
            }
            else
            {
                model = dao.ListDonHangDaNhan(searchString);
                if (model.Count == 0)
                {
                    SetAlert("Không tìm thấy đơn hàng " + searchString, "warning");
                }
            }
            ViewBag.Tong = model.Count();
            ViewBag.SoLuong = dao.ListDonHangDaNhan().Count();
            ViewBag.searchString = searchString;
            return View(model.ToPagedList(page, pagesize));
        }

        public ActionResult Donhuy(string searchString, int page = 1, int pagesize = 10)
        {
            if (QuyenNVBH())
            {
                return RedirectToAction("Index", "Home");
            }
            List<DonHang> model;
            if (string.IsNullOrEmpty(searchString))
            {
                model = dao.ListDonHangDaHuy();
            }
            else
            {
                model = dao.ListDonHangDaHuy(searchString);
                if (model.Count == 0)
                {
                    SetAlert("Không tìm thấy đơn hàng " + searchString, "warning");
                }
            }
            ViewBag.Tong = model.Count();
            ViewBag.SoLuong = dao.ListDonHangDaHuy().Count();
            ViewBag.searchString = searchString;
            return View(model.ToPagedList(page, pagesize));
        }

        public ActionResult Details(string madonhang)
        {
            DonHang model = dao.InfoDonHang(madonhang);
            if (model != null || string.IsNullOrEmpty(madonhang))
            {
                ArrayList dh = new ArrayList();
                var chitietdonhang = dao.ListChiTietDonHang(madonhang);
                foreach (var item in chitietdonhang)
                {
                    var sanpham = dao.InfoSanPham(item.maSP);
                    CartModel ca = new CartModel()
                    {
                        MaSP = sanpham.maSP,
                        TenSP = sanpham.tenSP,
                        MauSac = item.mauSac,
                        GiaBan = item.thanhTien,
                        AnhSP = sanpham.anhSP
                    };
                    dh.Add(ca);
                }
                ViewBag.ChiTietDonHang = dh;
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult Update(string madonhang, string trangthai, string lydo)
        {
            if (QuyenNVBH())
            {
                return RedirectToAction("Index", "Home");
            }
            DonHang model = dao.InfoDonHang(madonhang);
            if (model == null)
            {
                return RedirectToAction("Index");
            }
            LoginModel session = (LoginModel)Session[Constants.ADMIN_SESSION];
            if (session != null)
            {
                bool result = dao.UpdateDonHang(madonhang, trangthai, lydo, session.MaNV);
                if (result)
                {
                    SetAlert("Cập nhật đơn hàng thành công!", "success");
                    return RedirectToAction("Index");
                }
                else
                {
                    SetAlert("Cập nhật đơn hàng thất bại!", "danger");
                }
            }
            return RedirectToAction("Index");
        }

        public ActionResult Danhanhang(string madonhang)
        {
            if (QuyenNVBH())
            {
                return RedirectToAction("Index", "Home");
            }
            DonHang model = dao.InfoDonHang(madonhang);
            if (model == null)
            {
                return RedirectToAction("Index");
            }
            LoginModel session = (LoginModel)Session[Constants.ADMIN_SESSION];
            if (session != null)
            {
                bool result = dao.XacNhanDonHangDaNhan(madonhang, session.MaNV);
                if (result)
                {
                    SetAlert("Cập nhật đơn hàng thành công!", "success");
                }
                else
                {
                    SetAlert("Cập nhật đơn hàng thất bại!", "danger");
                }
            }
            return RedirectToAction("Details", new { madonhang });
        }

    }
}