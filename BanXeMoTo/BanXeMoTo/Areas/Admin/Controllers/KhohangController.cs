﻿using BanXeMoTo.Areas.Admin.Models;
using BanXeMoTo.Common;
using Models.DAO;
using Models.EF;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BanXeMoTo.Areas.Admin.Controllers
{
    public class KhohangController : BaseController
    {
        private AdminDao dao = new AdminDao();

        // GET: Admin/Khohang
        public ActionResult Index()
        {
            if (QuyenNVKH())
            {
                return RedirectToAction("Index", "Home");
            }
            return View();
        }

        public ActionResult Nhapkho(string searchString, int page = 1, int pagesize = 10)
        {
            if (QuyenNVKH())
            {
                return RedirectToAction("Index", "Home");
            }
            List<PhieuNhap> model;
            if (string.IsNullOrEmpty(searchString))
            {
                model = dao.ListPhieuNhap();
                foreach (var item in model)
                {
                    dao.TinhSoLuongPhieuNhap(item.maPN);
                }
            }
            else
            {
                model = dao.ListPhieuNhap(searchString);
                if (model.Count == 0)
                {
                    SetAlert("Không tìm thấy " + searchString, "warning");
                }
            }
            ViewBag.Tong = model.Count();
            ViewBag.SoLuong = dao.ListPhieuNhap().Count();
            ViewBag.searchString = searchString;
            return View(model.ToPagedList(page, pagesize));
        }

        public ActionResult Detailspn(string maphieunhap)
        {
            if (QuyenNVKH())
            {
                return RedirectToAction("Index", "Home");
            }
            PhieuNhap model = dao.InfoPhieuNhap(maphieunhap);
            if (model == null)
            {
                return RedirectToAction("Nhapkho");
            }
            ViewBag.NgayNhap = String.Format("{0:dd/MM/yyyy}", DateTime.Now);
            return View(model);
        }

        public ActionResult Detailspx(string madonhang)
        {
            if (QuyenNVKH())
            {
                return RedirectToAction("Index", "Home");
            }
            var model = dao.InfoDonHang(madonhang);
            if (model == null)
            {
                return RedirectToAction("Xuatkho");
            }
            return View(model);
        }

        public ActionResult Create()
        {
            if (QuyenNVKH())
            {
                return RedirectToAction("Index", "Home");
            }
            var Listncc = dao.DanhSachNhaCungCap();
            ViewBag.ListNhaCungCap = new SelectList(Listncc, "maNCC", "tenNCC");
            ViewBag.MaPhieuNhap = dao.TaoMaPhieuNhap();
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(PhieuNhap phieunhap)
        {
            if (QuyenNVKH())
            {
                return RedirectToAction("Index", "Home");
            }
            var Listncc = dao.DanhSachNhaCungCap();
            ViewBag.ListNhaCungCap = new SelectList(Listncc, "maNCC", "tenNCC");
            ViewBag.MaPhieuNhap = dao.TaoMaPhieuNhap();
            LoginModel session = (LoginModel)Session[Constants.ADMIN_SESSION];
            if (ModelState.IsValid)
            {
                phieunhap.maPN = ViewBag.MaPhieuNhap;
                phieunhap.maNV = session.MaNV;
                phieunhap.ngayNhap = DateTime.Now;
                bool model = dao.InsertPhieuNhap(phieunhap);
                if (model)
                {
                    SetAlert("Thành công, hãy nhập thông tin sản phẩm", "success");
                    return RedirectToAction("Createttsp", new { @maphieunhap = phieunhap.maPN });
                }
                else
                {
                    SetAlert("Thêm thất bại! Hãy kiểm tra lại thông tin", "danger");
                }
            }
            return View(phieunhap);
        }

        public ActionResult Createttsp(string maphieunhap)
        {
            if (QuyenNVKH())
            {
                return RedirectToAction("Index", "Home");
            }
            var Listsp = dao.ListSanPham();
            ViewBag.ListSanPham = new SelectList(Listsp, "maSP", "tenSP");
            ViewBag.MaPhieuNhap = maphieunhap;
            ViewBag.MaTTSP = dao.TaoMaThongTinSanPham();
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Createttsp(ThongTinSanPham ttsp, string luuvaket)
        {
            if (QuyenNVKH())
            {
                return RedirectToAction("Index", "Home");
            }
            var Listsp = dao.ListSanPham();
            ViewBag.ListSanPham = new SelectList(Listsp, "maSP", "tenSP");
            ViewBag.MaPhieuNhap = ttsp.maPN;

            if (ModelState.IsValid)
            {
                ttsp.maTTSP = dao.TaoMaThongTinSanPham();
                string result = dao.InsertThongTinSanPham(ttsp);
                if (result.Equals("done"))
                {
                    dao.TinhTienPhieuNhap(ttsp.maPN);
                    SetAlert("Thêm sản phẩm thành công", "success");
                    if (luuvaket != null)
                    {
                        return RedirectToAction("Nhapkho");
                    }
                    return RedirectToAction("Createttsp");
                }
                else
                {
                    SetAlert(ThongBaoLoi(result), "danger");
                }
            }
            return View(ttsp);
        }

        public ActionResult Xuatkho(string searchString, int page = 1, int pagesize = 10)
        {
            if (QuyenNVKH())
            {
                return RedirectToAction("Index", "Home");
            }
            List<DonHang> model;
            if (string.IsNullOrEmpty(searchString))
            {
                model = dao.ListDonHangDaXuatKho();
            }
            else
            {
                model = dao.ListDonHangDaXuatKho(searchString);
                if (model.Count == 0)
                {
                    SetAlert("Không tìm thấy " + searchString, "warning");
                }
            }
            ViewBag.Tong = model.Count();
            ViewBag.SoLuong = dao.ListDonHangDaXuatKho().Count();
            ViewBag.searchString = searchString;
            return View(model.ToPagedList(page, pagesize));
        }

        public ActionResult Xuatttsp(string searchString, int page = 1, int pagesize = 10)
        {
            if (QuyenNVKH())
            {
                return RedirectToAction("Index", "Home");
            }
            List<DonHang> model;
            if (string.IsNullOrEmpty(searchString))
            {
                model = dao.ListDonHangDaXacNhan();
            }
            else
            {
                model = dao.ListDonHangDaXacNhan(searchString);
                if (model.Count == 0)
                {
                    SetAlert("Không tìm thấy đơn hàng " + searchString, "warning");
                }
            }
            ViewBag.Tong = model.Count();
            ViewBag.SoLuong = dao.ListDonHangDaXacNhan().Count();
            ViewBag.searchString = searchString;
            return View(model.ToPagedList(page, pagesize));
        }

        [HttpPost]
        public ActionResult Xacnhan(string madonhang)
        {
            if (QuyenNVKH())
            {
                return RedirectToAction("Index", "Home");
            }
            DonHang model = dao.InfoDonHang(madonhang);
            if (model == null)
            {
                return RedirectToAction("Index");
            }
            LoginModel session = (LoginModel)Session[Constants.ADMIN_SESSION];
            if (session != null)
            {
                bool result = dao.XacNhanXuatDonHang(madonhang, session.MaNV);
                if (result)
                {
                    SetAlert("Cập nhật đơn hàng thành công!", "success");
                    return RedirectToAction("Xuatttsp");
                }
                else
                {
                    SetAlert("Cập nhật đơn hàng thất bại!", "danger");
                }
            }
            return RedirectToAction("Xuatttsp");
        }

        public ActionResult Updatepn(string mattsp)
        {
            if (QuyenNVKH())
            {
                return RedirectToAction("Index", "Home");
            }
            var Listsp = dao.ListSanPham();
            ViewBag.ListSanPham = new SelectList(Listsp, "maSP", "tenSP");
            ThongTinSanPham model = dao.InfoThongTinSanPham(mattsp);
            if (model == null)
            {
                return RedirectToAction("Index");
            }
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Updatepn(ThongTinSanPham ttsp)
        {
            if (QuyenNVKH())
            {
                return RedirectToAction("Index", "Home");
            }
            var Listsp = dao.ListSanPham();
            ViewBag.ListSanPham = new SelectList(Listsp, "maSP", "tenSP");
            if (ModelState.IsValid)
            {
                ThongTinSanPham model = dao.InfoThongTinSanPham(ttsp.maTTSP);
                if (model == null)
                {
                    return RedirectToAction("Index");
                }
                bool result = dao.UpdateThongTinSanPhamPN(ttsp);
                if (result)
                {
                    dao.TinhTienPhieuNhap(model.maPN);
                    SetAlert("Cập nhật thông tin sản phẩm thành công!", "success");
                    return RedirectToAction("Detailspn", new { @maphieunhap = model.maPN });
                }
                else
                {
                    SetAlert("Cập nhật thông tin sản phẩm thất bại! Hãy kiểm tra lại thông tin", "danger");
                }
            }
            return View(ttsp);
        }

    }
}