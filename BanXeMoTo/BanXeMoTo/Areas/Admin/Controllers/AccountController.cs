﻿using BanXeMoTo.Areas.Admin.Models;
using BanXeMoTo.Common;
using Models.DAO;
using Models.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BanXeMoTo.Areas.Admin.Controllers
{
    public class AccountController : BaseController
    {
        private AdminDao dao = new AdminDao();

        // GET: Admin/Account
        public ActionResult Index()
        {
            LoginModel session = (LoginModel)Session[Constants.ADMIN_SESSION];
            if (session != null)
            {
                var model = dao.InfoNhanVien(session.MaNV);
                return View(model);
            }
            return View();
        }

        public ActionResult Doimatkhau()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Doimatkhau(string matkhaucu, string matkhaumoi, string matkhaumoi2)
        {
            LoginModel session = (LoginModel)Session[Constants.ADMIN_SESSION];
            if (ModelState.IsValid)
            {
                if (session != null)
                {
                    if (session.MatKhau != matkhaucu)
                    {
                        SetAlert("Mật khẩu cũ không chính xác", "warning");
                    }
                    else if (matkhaumoi != matkhaumoi2)
                    {
                        SetAlert("Mật khẩu mới nhập lại không chính xác", "warning");
                    }
                    else
                    {
                        var model = dao.DoiMatKhau(session.MaNV, Encryptor.EncryptMD5(matkhaumoi), matkhaumoi);
                        if (model)
                        {
                            session.MatKhau = matkhaumoi;
                            SetAlert("Đổi mật khẩu thành công", "success");
                            return RedirectToAction("Index");
                        }
                        else
                        {
                            SetAlert("Đổi mật khẩu thất bại", "danger");
                        }
                    }
                }
            }
            return View();
        }

        public ActionResult Suathongtin()
        {
            LoginModel session = (LoginModel)Session[Constants.ADMIN_SESSION];
            if (session != null)
            {
                var model = dao.InfoNhanVien(session.MaNV);
                return View(model);
            }
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Suathongtin(NhanVien model, int day, int month, int year)
        {
            LoginModel session = (LoginModel)Session[Constants.ADMIN_SESSION];
            if (ModelState.IsValid)
            {
                if (session != null)
                {
                    model.maNV = session.MaNV;
                    DateTime ngaysinh = new DateTime(year, month, day);
                    model.ngaySinh = ngaysinh;
                    var result = dao.CapNhatThongTin(model);
                    if (result)
                    {
                        SetAlert("Cập nhật thông tin thành công", "success");
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        SetAlert("Cập nhật thông tin thất bại", "error");
                    }
                }
            }
            return View();
        }

    }
}