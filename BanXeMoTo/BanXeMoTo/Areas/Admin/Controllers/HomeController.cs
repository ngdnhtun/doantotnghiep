﻿using BanXeMoTo.Common;
using Microsoft.Ajax.Utilities;
using Models.DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;

namespace BanXeMoTo.Areas.Admin.Controllers
{
    public class HomeController : BaseController
    {
        private AdminDao dao = new AdminDao();

        // GET: Admin/Home
        public ActionResult Index()
        {
            //đoạn code chạy lần đầu

            //var model = dao.ListPhieuNhap();
            //foreach (var item in model)
            //{
            //    dao.TinhTienPhieuNhap(item.maPN);
            //    dao.TinhTienGiaBan(item.maPN);
            //}
            //var model1 = dao.ListSanPham();
            //foreach (var item in model1)
            //{
            //    dao.TinhSoLuong(item.maSP);
            //}
            //dao.ListSanPhamAutoUpdate();

            //end
            return View();
        }

        public ActionResult Logout()
        {
            Session[Constants.ADMIN_SESSION] = null;
            return RedirectToAction("Index", "Login");
        }
    }
}