﻿using Models.DAO;
using Models.EF;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BanXeMoTo.Areas.Admin.Controllers
{
    public class ManageController : BaseController
    {
        private AdminDao dao = new AdminDao();

        // Nhà cung cấp
        public ActionResult Supplier(int page = 1, int pagesize = 10)
        {
            if (QuyenAdmin())
            {
                return RedirectToAction("Index", "Home");
            }
            var model = dao.ListNhaCungCap();
            return View(model.ToPagedList(page, pagesize));
        }

        [HttpPost]
        public ActionResult Supplier(string searchString, int page = 1, int pagesize = 10)
        {
            if (QuyenAdmin())
            {
                return RedirectToAction("Index", "Home");
            }
            var model = dao.ListNhaCungCap(searchString);
            if (model.Count == 0)
            {
                SetAlert("Không tìm thấy " + searchString, "warning");
            }
            ViewBag.searchString = searchString;
            return View(model.ToPagedList(page, pagesize));
        }

        // Chức vụ
        public ActionResult Position(int page = 1, int pagesize = 10)
        {
            if (QuyenAdmin())
            {
                return RedirectToAction("Index", "Home");
            }
            var model = dao.ListChucVu();
            return View(model.ToPagedList(page, pagesize));
        }

        [HttpPost]
        public ActionResult Position(string searchString, int page = 1, int pagesize = 10)
        {
            if (QuyenAdmin())
            {
                return RedirectToAction("Index", "Home");
            }
            var model = dao.ListChucVu(searchString);
            if (model.Count == 0)
            {
                SetAlert("Không tìm thấy " + searchString, "warning");
            }
            ViewBag.searchString = searchString;
            return View(model.ToPagedList(page, pagesize));
        }

        // Bình luận đánh giá
        public ActionResult Comment(int page = 1, int pagesize = 10)
        {
            if (QuyenAdmin())
            {
                return RedirectToAction("Index", "Home");
            }
            var model = dao.ListBinhLuanDanhGia();
            return View(model.ToPagedList(page, pagesize));
        }

        [HttpPost]
        public ActionResult Comment(string searchString, int page = 1, int pagesize = 10)
        {
            if (QuyenAdmin())
            {
                return RedirectToAction("Index", "Home");
            }
            var model = dao.ListBinhLuanDanhGia(searchString);
            if (model.Count == 0)
            {
                SetAlert("Không tìm thấy " + searchString, "warning");
            }
            ViewBag.searchString = searchString;
            return View(model.ToPagedList(page, pagesize));
        }

        public ActionResult Updatecomment(string makh, string masp)
        {
            if (QuyenAdmin())
            {
                return RedirectToAction("Index", "Home");
            }
            var model = dao.InfoBinhLuanDanhGia(makh, masp);
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Updatecomment(BinhLuanDanhGia bldg)
        {
            if (QuyenAdmin())
            {
                return RedirectToAction("Index", "Home");
            }
            if (ModelState.IsValid)
            {
                BinhLuanDanhGia model = dao.InfoBinhLuanDanhGia(bldg.maKH, bldg.maSP);
                if (model == null)
                {
                    return RedirectToAction("Comment");
                }
                bool result = dao.UpdateBinhLuanDanhGia(bldg);
                if (result)
                {
                    SetAlert("Cập nhật đánh giá thành công!", "success");
                }
                else
                {
                    SetAlert("Cập nhật đánh giá thất bại", "danger");
                }
            }
            return RedirectToAction("Updatecomment", new { bldg.maKH, bldg.maSP });
        }

    }
}