﻿using BanXeMoTo.Areas.Admin.Models;
using BanXeMoTo.Common;
using Models.DAO;
using Models.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BanXeMoTo.Areas.Admin.Controllers
{
    public class LoginController : Controller
    {
        private AdminDao dao = new AdminDao();

        // GET: Admin/Login
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(LoginModel user)
        {
            if (ModelState.IsValid)
            {
                int result = dao.Login(user.TaiKhoan, Encryptor.EncryptMD5(user.MatKhau));
                //(user.MatKhau)
                if (result == 1)
                {
                    NhanVien info = dao.InfoNhanVien(user.TaiKhoan);
                    if (!string.IsNullOrEmpty(info.trangThai))
                    {
                        TempData["AlertType"] = "alert-danger";
                        TempData["AlertMessage"] = "Tài khoản của bạn đã bị khóa!";
                        return RedirectToAction("Index", "Login");
                    }
                    user.MaNV = info.maNV;
                    user.TenNV = info.tenNV;
                    user.TenCV = info.ChucVu.tenCV;
                    user.Quyen = info.ChucVu.quyenCV;
                    user.GioiTinh = info.gioiTinh;

                    Session.Add(Constants.ADMIN_SESSION, user);
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    TempData["AlertType"] = "alert-danger";
                    TempData["AlertMessage"] = "Sai thông tin đăng nhập!";
                }
            }
            return View("Index");
        }

        public ActionResult Quenmatkhau()
        {
            return View();
        }
    }
}