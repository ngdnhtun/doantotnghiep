﻿using BanXeMoTo.Areas.Admin.Models;
using BanXeMoTo.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace BanXeMoTo.Areas.Admin.Controllers
{
    public class BaseController : Controller
    {
        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            LoginModel session = (LoginModel)Session[Constants.ADMIN_SESSION];
            if (session == null)
            {
                filterContext.Result = new RedirectToRouteResult(new
                    RouteValueDictionary(new { Controller = "Login", action = "Index", Areas = "Admin" }));
            }
            base.OnActionExecuted(filterContext);
        }

        protected bool QuyenAdmin()
        {
            LoginModel session = (LoginModel)Session[Constants.ADMIN_SESSION];
            if (session != null)
            {
                if (session.Quyen == "1" || session.Quyen == "2")
                {
                    SetAlert("Bạn không có quyền truy cập chức năng này!", "warning");
                    return true;
                }
            }
            return false;
        }

        protected bool QuyenNVBH()
        {
            LoginModel session = (LoginModel)Session[Constants.ADMIN_SESSION];
            if (session != null)
            {
                if (session.Quyen == "2")
                {
                    SetAlert("Bạn không có quyền truy cập chức năng này!", "warning");
                    return true;
                }
            }
            return false;
        }

        protected bool QuyenNVKH()
        {
            LoginModel session = (LoginModel)Session[Constants.ADMIN_SESSION];
            if (session != null)
            {
                if (session.Quyen == "1")
                {
                    SetAlert("Bạn không có quyền truy cập chức năng này!", "warning");
                    return true;
                }
            }
            return false;
        }

        protected void SetAlert(string message, string type)
        {
            TempData["AlertMessage"] = message;
            if (type == "success")
            {
                TempData["AlertType"] = "alert-success";
            }
            else if (type == "warning")
            {
                TempData["AlertType"] = "alert-warning";
            }
            else if (type == "danger")
            {
                TempData["AlertType"] = "alert-danger";
            }
        }

        protected string ThongBaoLoi(string bug)
        {
            switch (bug)
            {
                //table Khach Hang, Nhan Vien
                case "SDT": return "Số điện thoại bạn nhập đã tồn tại.";
                case "Email": return "Email bạn nhập đã tồn tại.";
                case "CMND": return "Số CMND bạn nhập đã tồn tại.";
                //table Thong Tin San Pham
                case "soKhung": return "Số khung bạn nhập đã tồn tại.";
                case "soMay": return "Số máy bạn nhập đã tồn tại.";

                default: return "Có lỗi xảy ra khi thực hiên, vui lòng thử lại sau.";
            }
        }
    }
}