﻿using Models.DAO;
using Models.EF;
using PagedList;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BanXeMoTo.Areas.Admin.Controllers
{
    public class SanphamController : BaseController
    {
        private AdminDao dao = new AdminDao();

        // GET: Admin/Sanpham
        public ActionResult Index(string searchString, int page = 1, int pagesize = 10)
        {
            List<SanPham> model;
            if (string.IsNullOrEmpty(searchString))
            {

                model = dao.ListSanPham();
            }
            else
            {
                model = dao.ListSanPham(searchString);
                if (model.Count == 0)
                {
                    SetAlert("Không tìm thấy " + searchString, "warning");
                }
            }
            ViewBag.Tong = model.Count();
            ViewBag.SoLuong = dao.ListSanPham().Count();
            ViewBag.searchString = searchString;
            return View(model.ToPagedList(page, pagesize));
        }

        public ActionResult Refreshsp()
        {
            bool kt = dao.ListSanPhamAutoUpdate();
            if (kt)
            {
                SetAlert("Refresh sản phẩm thành công", "success");
            }
            else
            {
                SetAlert("Refresh sản phẩm thất bại", "danger");
            }
            return RedirectToAction("Index"); ;
        }

        public ActionResult Details(string masanpham)
        {
            SanPham model = dao.InfoSanPham(masanpham);
            if (model == null)
            {
                return RedirectToAction("Index");
            }
            return View(model);
        }

        public ActionResult Update(string masanpham)
        {
            if (QuyenNVKH())
            {
                return RedirectToAction("Index", "Home");
            }
            SanPham model = dao.InfoSanPham(masanpham);
            if (model == null)
            {
                return RedirectToAction("Index");
            }
            ViewBag.ListLoaiSanPham = dao.ListLoaiSanPham();
            ViewBag.ListHangSanXuat = dao.ListHangSanXuat();
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Update(SanPham sanpham, string lsp, HttpPostedFileBase file, HttpPostedFileBase file1, HttpPostedFileBase file2, HttpPostedFileBase file3, HttpPostedFileBase file4, HttpPostedFileBase file5)
        {
            if (QuyenNVKH())
            {
                return RedirectToAction("Index", "Home");
            }
            ViewBag.ListLoaiSanPham = dao.ListLoaiSanPham();
            ViewBag.ListHangSanXuat = dao.ListHangSanXuat();
            sanpham.maLSP = lsp;
            if (ModelState.IsValid)
            {
                string fileName, fileName1, fileName2, fileName3, fileName4, fileName5;
                string fullPath = "", fullPath1 = "", fullPath2 = "", fullPath3 = "", fullPath4 = "", fullPath5 = "";
                string path = Server.MapPath("~/Assets/user/products-images");

                if (file != null)
                {
                    fileName = Path.GetFileName(file.FileName);
                    fullPath = Path.Combine(path, fileName);
                    sanpham.anhSP = fileName;
                }
                if (file1 != null)
                {
                    fileName1 = Path.GetFileName(file1.FileName);
                    fullPath1 = Path.Combine(path, fileName1);
                    sanpham.hinhAnh1 = fileName1;
                }
                if (file2 != null)
                {
                    fileName2 = Path.GetFileName(file2.FileName);
                    fullPath2 = Path.Combine(path, fileName2);
                    sanpham.hinhAnh2 = fileName2;
                }
                if (file3 != null)
                {
                    fileName3 = Path.GetFileName(file3.FileName);
                    fullPath3 = Path.Combine(path, fileName3);
                    sanpham.hinhAnh3 = fileName3;
                }
                if (file4 != null)
                {
                    fileName4 = Path.GetFileName(file4.FileName);
                    fullPath4 = Path.Combine(path, fileName4);
                    sanpham.hinhAnh4 = fileName4;
                }
                if (file5 != null)
                {
                    fileName5 = Path.GetFileName(file5.FileName);
                    fullPath5 = Path.Combine(path, fileName5);
                    sanpham.hinhAnh5 = fileName5;
                }

                SanPham model = dao.InfoSanPham(sanpham.maSP);
                if (model == null)
                {
                    return RedirectToAction("Index");
                }
                bool result = dao.UpdateSanPham(sanpham);
                if (result)
                {
                    if (file != null) { file.SaveAs(fullPath); }
                    if (file1 != null) { file1.SaveAs(fullPath1); }
                    if (file2 != null) { file2.SaveAs(fullPath2); }
                    if (file3 != null) { file3.SaveAs(fullPath3); }
                    if (file4 != null) { file4.SaveAs(fullPath4); }
                    if (file5 != null) { file5.SaveAs(fullPath5); }

                    SetAlert("Cập nhật thành công", "success");
                    return RedirectToAction("Details", new { @masanpham = sanpham.maSP });
                }
                else
                {
                    SetAlert("Cập nhật thất bại! Hãy kiểm tra lại thông tin", "danger");
                }
            }
            return RedirectToAction("Update", new { @masanpham = sanpham.maSP });
        }

        public ActionResult Create()
        {
            if (QuyenNVKH())
            {
                return RedirectToAction("Index", "Home");
            }

            //var Listlsp = dao.ListLoaiSanPham();
            //ViewBag.ListLoaiSanPham = new SelectList(Listlsp, "maLSP", "tenLSP");
            //var Listhsx = dao.ListHangSanXuat();
            //ViewBag.ListHangSanXuat = new SelectList(Listhsx, "maHSX", "tenHSX");

            ViewBag.ListLoaiSanPham = dao.ListLoaiSanPham();
            ViewBag.ListHangSanXuat = dao.ListHangSanXuat();

            ViewBag.MaSanPham = dao.TaoMaSanPham();
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(SanPham sanpham, string lsp, HttpPostedFileBase file, HttpPostedFileBase file1, HttpPostedFileBase file2, HttpPostedFileBase file3, HttpPostedFileBase file4, HttpPostedFileBase file5)
        {
            if (QuyenNVKH())
            {
                return RedirectToAction("Index", "Home");
            }
            ViewBag.ListLoaiSanPham = dao.ListLoaiSanPham();
            ViewBag.ListHangSanXuat = dao.ListHangSanXuat();
            ViewBag.MaSanPham = dao.TaoMaSanPham();
            if (ModelState.IsValid)
            {
                string fileName1 = "", fileName2 = "", fileName3 = "", fileName4 = "", fileName5 = "";
                string fullPath1 = "", fullPath2 = "", fullPath3 = "", fullPath4 = "", fullPath5 = "";
                string path = Server.MapPath("~/Assets/user/products-images");

                string fileName = Path.GetFileName(file.FileName);
                string fullPath = Path.Combine(path, fileName);
                if (file1 != null)
                {
                    fileName1 = Path.GetFileName(file1.FileName);
                    fullPath1 = Path.Combine(path, fileName1);
                }
                if (file2 != null)
                {
                    fileName2 = Path.GetFileName(file2.FileName);
                    fullPath2 = Path.Combine(path, fileName2);
                }
                if (file3 != null)
                {
                    fileName3 = Path.GetFileName(file3.FileName);
                    fullPath3 = Path.Combine(path, fileName3);
                }
                if (file4 != null)
                {
                    fileName4 = Path.GetFileName(file4.FileName);
                    fullPath4 = Path.Combine(path, fileName4);
                }
                if (file5 != null)
                {
                    fileName5 = Path.GetFileName(file5.FileName);
                    fullPath5 = Path.Combine(path, fileName5);
                }
                sanpham.maSP = ViewBag.MaSanPham;
                sanpham.maLSP = lsp;
                sanpham.giaBan = "Liên hệ";
                sanpham.soLuong = 0;
                sanpham.xemNhieu = 0;
                sanpham.muaNhieu = 0;
                sanpham.tiLeDanhGia = 0;
                sanpham.anhSP = fileName;
                sanpham.hinhAnh1 = fileName1;
                sanpham.hinhAnh2 = fileName2;
                sanpham.hinhAnh3 = fileName3;
                sanpham.hinhAnh4 = fileName4;
                sanpham.hinhAnh5 = fileName5;
                bool result = dao.InsertSanPham(sanpham);
                if (result)
                {
                    file.SaveAs(fullPath);
                    if (file1 != null) { file1.SaveAs(fullPath1); }
                    if (file2 != null) { file2.SaveAs(fullPath2); }
                    if (file3 != null) { file3.SaveAs(fullPath3); }
                    if (file4 != null) { file4.SaveAs(fullPath4); }
                    if (file5 != null) { file5.SaveAs(fullPath5); }

                    SetAlert("Thêm sản phẩm thành công", "success");
                    return RedirectToAction("Details", new { @masanpham = sanpham.maSP });
                }
                else
                {
                    SetAlert("Thêm sản phẩm thất bại! Hãy kiểm tra lại thông tin", "danger");
                }
            }
            return View(sanpham);
        }

        public ActionResult Thongtinsanpham(string searchString, int page = 1, int pagesize = 10)
        {
            IEnumerable<ThongTinSanPham> model;
            if (string.IsNullOrEmpty(searchString))
            {
                model = dao.ListThongTinSanPham().Where(x => !x.tinhTrang.Contains("Đã bán"));
            }
            else
            {
                model = dao.ListThongTinSanPham(searchString).Where(x => !x.tinhTrang.Contains("Đã bán"));
                if (model.Count() == 0)
                {
                    SetAlert("Không tìm thấy " + searchString, "warning");
                }
            }
            ViewBag.Tong = model.Count();
            ViewBag.SoLuong = dao.ListThongTinSanPham().Where(x => !x.tinhTrang.Contains("Đã bán")).Count();
            ViewBag.searchString = searchString;
            return View(model.ToPagedList(page, pagesize));
        }

        public ActionResult Detailsttsp(string mattsp)
        {
            if (QuyenNVKH())
            {
                return RedirectToAction("Index", "Home");
            }
            var Listsp = dao.ListSanPham();
            ViewBag.ListSanPham = new SelectList(Listsp, "maSP", "tenSP");
            ThongTinSanPham model = dao.InfoThongTinSanPham(mattsp);
            if (model == null)
            {
                return RedirectToAction("Index");
            }
            return View(model);
        }

        public ActionResult Sanphamdaban(string searchString, int page = 1, int pagesize = 10)
        {
            IEnumerable<ThongTinSanPham> model;
            if (string.IsNullOrEmpty(searchString))
            {
                model = dao.ListThongTinSanPham().Where(x => x.tinhTrang.Contains("Đã bán"));
            }
            else
            {
                model = dao.ListThongTinSanPham(searchString).Where(x => x.tinhTrang.Contains("Đã bán")); ;
                if (model.Count() == 0)
                {
                    SetAlert("Không tìm thấy " + searchString, "warning");
                }
            }
            ViewBag.Tong = model.Count();
            ViewBag.SoLuong = dao.ListThongTinSanPham().Where(x => x.tinhTrang.Contains("Đã bán")).Count();
            ViewBag.searchString = searchString;
            return View(model.ToPagedList(page, pagesize));
        }

        public ActionResult Updatettsp(string mattsp)
        {
            if (QuyenNVKH())
            {
                return RedirectToAction("Index", "Home");
            }
            var Listsp = dao.ListSanPham();
            ViewBag.ListSanPham = new SelectList(Listsp, "maSP", "tenSP");
            ThongTinSanPham model = dao.InfoThongTinSanPham(mattsp);
            if (model == null)
            {
                return RedirectToAction("Index");
            }
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Updatettsp(ThongTinSanPham ttsp)
        {
            if (QuyenNVKH())
            {
                return RedirectToAction("Index", "Home");
            }
            var Listsp = dao.ListSanPham();
            ViewBag.ListSanPham = new SelectList(Listsp, "maSP", "tenSP");

            if (ModelState.IsValid)
            {
                ThongTinSanPham model = dao.InfoThongTinSanPham(ttsp.maTTSP);
                if (model == null)
                {
                    return RedirectToAction("Index");
                }
                string result = dao.UpdateThongTinSanPham(ttsp);
                if (result.Equals("done"))
                {
                    SetAlert("Cập nhật thông tin sản phẩm thành công!", "success");
                    return RedirectToAction("Updatettsp", new { mattsp = ttsp.maTTSP });
                }
                else
                {
                    SetAlert(ThongBaoLoi(result), "danger");
                }
            }
            return View(ttsp);
        }

        public ActionResult Khongban(string masanpham)
        {
            if (QuyenNVKH())
            {
                return RedirectToAction("Index", "Home");
            }
            SanPham model = dao.InfoSanPham(masanpham);
            if (model == null)
            {
                return RedirectToAction("Index");
            }
            bool result = dao.KhongBanSanPham(masanpham);
            if (result)
            {
                SetAlert("Đã kháo không bán sản phẩm!", "success");
            }
            else
            {
                SetAlert("Mở bán sản phẩm thất bại! Hãy kiểm tra lại thông tin", "danger");
            }
            return RedirectToAction("Details", new { masanpham });
        }

        public ActionResult HuyKhongban(string masanpham)
        {
            if (QuyenNVKH())
            {
                return RedirectToAction("Index", "Home");
            }
            SanPham model = dao.InfoSanPham(masanpham);
            if (model == null)
            {
                return RedirectToAction("Index");
            }
            bool result = dao.HuyKhongBanSanPham(masanpham);
            if (result)
            {
                SetAlert("Đã mở bán sản phẩm!", "success");
            }
            else
            {
                SetAlert("Mở bán sản phẩm thất bại! Hãy kiểm tra lại thông tin", "danger");
            }
            return RedirectToAction("Details", new { masanpham });
        }

    }
}