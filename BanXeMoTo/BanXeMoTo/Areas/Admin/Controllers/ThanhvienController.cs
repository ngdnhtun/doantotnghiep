﻿using BanXeMoTo.Common;
using Models.DAO;
using Models.EF;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BanXeMoTo.Areas.Admin.Controllers
{
    public class ThanhvienController : BaseController
    {
        private AdminDao dao = new AdminDao();

        // GET: Admin/Thanhvien
        public ActionResult Index(string searchString, int page = 1, int pagesize = 10)
        {
            if (QuyenNVBH())
            {
                return RedirectToAction("Index", "Home");
            }
            List<KhachHang> model;
            if (string.IsNullOrEmpty(searchString))
            {
                model = dao.ListThanhVien();
            }
            else
            {
                model = dao.ListThanhVien(searchString);
                if (model.Count == 0)
                {
                    SetAlert("Không tìm thấy " + searchString, "warning");
                }
            }
            ViewBag.Tong = model.Count();
            ViewBag.SoLuong = dao.ListThanhVien().Count();
            ViewBag.searchString = searchString;
            return View(model.ToPagedList(page, pagesize));
        }

        public ActionResult Create()
        {
            if (QuyenNVBH())
            {
                return RedirectToAction("Index", "Home");
            }
            ViewBag.MaThanhVien = dao.TaoMaThanhVien();
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(KhachHang model, int year, int month, int day)
        {
            if (QuyenNVBH())
            {
                return RedirectToAction("Index", "Home");
            }
            ViewBag.MaThanhVien = dao.TaoMaThanhVien();
            if (ModelState.IsValid)
            {
                model.maKH = ViewBag.MaThanhVien;
                model.nickName = "12345678";
                model.matKhau = Encryptor.EncryptMD5(model.nickName);
                DateTime ngaysinh = new DateTime(year, month, day);
                model.ngaySinh = ngaysinh;
                model.tienThuong = 0;

                string result = dao.InsertThanhVien(model);
                if (result.Equals("done"))
                {
                    SetAlert("Đã tạo thành viên mới thành công", "success");
                    return RedirectToAction("Details", new { @mathanhvien = model.maKH });
                }
                else
                {
                    SetAlert(ThongBaoLoi(result), "danger");
                }
            }
            return View(model);
        }

        public ActionResult Details(string mathanhvien)
        {
            if (QuyenNVBH())
            {
                return RedirectToAction("Index", "Home");
            }
            KhachHang model = dao.InfoThanhVien(mathanhvien);
            if (model == null)
            {
                return RedirectToAction("Index");
            }
            return View(model);
        }

        public ActionResult Update(string mathanhvien)
        {
            if (QuyenNVBH())
            {
                return RedirectToAction("Index", "Home");
            }
            KhachHang model = dao.InfoThanhVien(mathanhvien);
            if (model == null)
            {
                return RedirectToAction("Index");
            }
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Update(KhachHang khachhang, int year, int month, int day)
        {
            if (QuyenNVBH())
            {
                return RedirectToAction("Index", "Home");
            }
            DateTime ngaysinh = new DateTime(year, month, day);
            khachhang.ngaySinh = ngaysinh;
            if (ModelState.IsValid)
            {
                KhachHang model = dao.InfoThanhVien(khachhang.maKH);
                if (model == null)
                {
                    return RedirectToAction("Index");
                }
                khachhang.nickName = model.nickName;
                //kiem tra mat khau co thay doi hay khong
                bool checkpass = dao.CheckPassThanhVien(model.maKH, khachhang.matKhau);
                if (checkpass)
                {
                    khachhang.nickName = khachhang.matKhau;
                    khachhang.matKhau = Encryptor.EncryptMD5(khachhang.matKhau);
                }

                string result = dao.UpdateThanhVien(khachhang);
                if (result.Equals("done"))
                {
                    SetAlert("Cập nhật nhân viên thành công!", "success");
                    return RedirectToAction("Details", new { @mathanhvien = model.maKH });
                }
                else
                {
                    SetAlert(ThongBaoLoi(result), "danger");
                }
            }
            return View(khachhang);
        }

        public ActionResult Khoataikhoan(string mathanhvien)
        {
            if (QuyenNVBH())
            {
                return RedirectToAction("Index", "Home");
            }
            KhachHang model = dao.InfoThanhVien(mathanhvien);
            if (model == null)
            {
                return RedirectToAction("Index");
            }
            bool result = dao.KhoaThanhVien(mathanhvien);
            if (result)
            {
                SetAlert("Đã khóa tài khoản", "warning");
            }
            else
            {
                SetAlert("Khóa thất bại! Hãy kiểm tra lại thông tin", "danger");
            }
            return RedirectToAction("Details", new { mathanhvien });
        }

        public ActionResult Huykhoataikhoan(string mathanhvien)
        {
            if (QuyenNVBH())
            {
                return RedirectToAction("Index", "Home");
            }
            KhachHang model = dao.InfoThanhVien(mathanhvien);
            if (model == null)
            {
                return RedirectToAction("Index");
            }
            bool result = dao.HuyKhoaThanhVien(mathanhvien);
            if (result)
            {
                SetAlert("Đã mở khóa tài khoản", "success");
            }
            else
            {
                SetAlert("Mở khóa thất bại! Hãy kiểm tra lại thông tin", "danger");
            }
            return RedirectToAction("Details", new { mathanhvien });
        }

    }
}