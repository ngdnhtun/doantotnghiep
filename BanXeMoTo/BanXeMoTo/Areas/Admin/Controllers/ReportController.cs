﻿using Models.DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BanXeMoTo.Areas.Admin.Controllers
{
    public class ReportController : BaseController
    {
        private AdminDao dao = new AdminDao();

        // GET: Admin/Report
        public ActionResult Index()
        {
            if (QuyenAdmin())
            {
                return RedirectToAction("Index", "Home");
            }
            var thang = DateTime.Now.Month;
            var nam = DateTime.Now.Year;
            ViewBag.Thang = "tháng " + thang;
            ViewBag.Nam = nam;
            ViewBag.SelectNam = dao.ListDonHang().Where(x => x.ngayDatHang != null && x.ngayDatHang.Value.Year != nam).OrderByDescending(x => x.ngayDatHang).Select(x => x.ngayDatHang.Value.Year).Distinct();


            var model = dao.ReportDonHang(thang, nam);
            ViewBag.TongDonHangDaDat = model.Count();
            ViewBag.TongSanPhamBanDuoc = model.Where(x => x.trangThai.Contains("Đã nhận")).Sum(x => x.tongSoLuong);
            ViewBag.TongDonHangBanDuoc = model.Where(x => x.trangThai.Contains("Đã nhận")).Count();
            ViewBag.TongDonHangBiHuy = model.Where(x => x.trangThai.Contains("Đã hủy")).Count();

            ViewBag.TongGiaTriSanPham = dao.TongGiaTriSanPham(thang, nam);
            ViewBag.TongGiaTriBanDuoc = model.Where(x => x.trangThai.Contains("Đã nhận")).Sum(x => x.tongTien);
            ViewBag.TongGiaTriLoiNhuan = ViewBag.TongGiaTriBanDuoc - ViewBag.TongGiaTriSanPham;

            return View(model.ToList());
        }

        [HttpPost]
        public ActionResult Index(int thang, int nam)
        {
            if (QuyenAdmin())
            {
                return RedirectToAction("Index", "Home");
            }
            if (thang == 0)
            {
                ViewBag.Thang = "cả ";
            }
            else
            {
                ViewBag.Thang = "tháng " + thang;
            }
            ViewBag.Nam = nam;
            ViewBag.SelectNam = dao.ListDonHang().Where(x => x.ngayDatHang != null && x.ngayDatHang.Value.Year != nam).OrderByDescending(x => x.ngayDatHang).Select(x => x.ngayDatHang.Value.Year).Distinct();

            var model = dao.ReportDonHang(thang, nam);
            ViewBag.TongDonHangDaDat = model.Count();
            ViewBag.TongSanPhamBanDuoc = model.Where(x => x.trangThai.Contains("Đã nhận")).Sum(x => x.tongSoLuong);
            ViewBag.TongDonHangBanDuoc = model.Where(x => x.trangThai.Contains("Đã nhận")).Count();
            ViewBag.TongDonHangBiHuy = model.Where(x => x.trangThai.Contains("Đã hủy")).Count();

            ViewBag.TongGiaTriSanPham = dao.TongGiaTriSanPham(thang, nam);
            ViewBag.TongGiaTriBanDuoc = model.Where(x => x.trangThai.Contains("Đã nhận")).Sum(x => x.tongTien);
            ViewBag.TongGiaTriLoiNhuan = ViewBag.TongGiaTriBanDuoc - ViewBag.TongGiaTriSanPham;

            return View(model.ToList());
        }

        public ActionResult Doanhson()
        {
            if (QuyenAdmin())
            {
                return RedirectToAction("Index", "Home");
            }
            var thang = DateTime.Now.Month;
            var nam = DateTime.Now.Year;
            ViewBag.Thang = "tháng " + thang;
            ViewBag.Nam = nam;
            ViewBag.SelectNam = dao.ListPhieuNhap().Where(x => x.ngayNhap != null && x.ngayNhap.Value.Year != nam).OrderByDescending(x => x.ngayNhap).Select(x => x.ngayNhap.Value.Year).Distinct();

            var model = dao.ReportNhap(thang, nam);
            int tspn = 0;
            decimal? tgtspn = 0;
            foreach (var item in model)
            {
                tspn += dao.ListThongTinSanPham().Where(x => x.maPN.Contains(item.maPN)).Count();
                tgtspn += dao.ListThongTinSanPham().Where(x => x.maPN.Contains(item.maPN)).Sum(x => x.giaGoc);
            }
            ViewBag.TongSanPhamTrongKho = dao.ListThongTinSanPham().Where(x => !x.tinhTrang.Contains("Đã bán")).Count();
            ViewBag.TongSanPhamDaNhap = tspn;
            ViewBag.TongPhieuNhapDaTao = model.Count();

            ViewBag.TongGiaTriSanPhamTrongKho = dao.ListThongTinSanPham().Where(x => !x.tinhTrang.Contains("Đã bán")).Sum(x => x.giaGoc);
            ViewBag.TongGiaTriSanPhamNhap = tgtspn;

            return View(model.ToList());
        }

        [HttpPost]
        public ActionResult Doanhson(int thang, int nam)
        {
            if (QuyenAdmin())
            {
                return RedirectToAction("Index", "Home");
            }
            if (thang == 0)
            {
                ViewBag.Thang = "cả ";
            }
            else
            {
                ViewBag.Thang = "tháng " + thang;
            }
            ViewBag.Nam = nam;
            ViewBag.SelectNam = dao.ListPhieuNhap().Where(x => x.ngayNhap != null && x.ngayNhap.Value.Year != nam).OrderByDescending(x => x.ngayNhap).Select(x => x.ngayNhap.Value.Year).Distinct();

            var model = dao.ReportNhap(thang, nam);
            int tspn = 0;
            decimal? tgtspn = 0;
            foreach (var item in model)
            {
                tspn += dao.ListThongTinSanPham().Where(x => x.maPN.Contains(item.maPN)).Count();
                tgtspn += dao.ListThongTinSanPham().Where(x => x.maPN.Contains(item.maPN)).Sum(x => x.giaGoc);
            }
            ViewBag.TongSanPhamTrongKho = dao.ListThongTinSanPham().Where(x => !x.tinhTrang.Contains("Đã bán")).Count();
            ViewBag.TongSanPhamDaNhap = tspn;
            ViewBag.TongPhieuNhapDaTao = model.Count();

            ViewBag.TongGiaTriSanPhamTrongKho = dao.ListThongTinSanPham().Where(x => !x.tinhTrang.Contains("Đã bán")).Sum(x=>x.giaGoc);
            ViewBag.TongGiaTriSanPhamNhap = tgtspn;

            return View(model.ToList());
        }

        public ActionResult Doanhsox()
        {
            if (QuyenAdmin())
            {
                return RedirectToAction("Index", "Home");
            }
            var thang = DateTime.Now.Month;
            var nam = DateTime.Now.Year;
            ViewBag.Thang = "tháng " + thang;
            ViewBag.Nam = nam;
            ViewBag.SelectNam = dao.ListDonHang().Where(x => x.ngayDatHang != null && x.ngayDatHang.Value.Year != nam).OrderByDescending(x => x.ngayDatHang).Select(x => x.ngayDatHang.Value.Year).Distinct();

            var model = dao.ReportDonHang(thang, nam).Where(x => x.trangThai.Contains("Đã nhận"));
            ViewBag.TongSanPhamTrongKho = dao.ListThongTinSanPham().Where(x => !x.tinhTrang.Contains("Đã bán")).Count();
            ViewBag.TongSanPhamDaXuat = model.Sum(x => x.tongSoLuong);

            ViewBag.TongGiaTriNhapVao = dao.TongGiaTriSanPham(thang, nam);
            ViewBag.TongGiaTriXuatRa = model.Sum(x => x.tongTien);
            ViewBag.TongGiaTriLoiNhuan = ViewBag.TongGiaTriXuatRa - ViewBag.TongGiaTriNhapVao;

            return View(model.ToList());
        }

        [HttpPost]
        public ActionResult Doanhsox(int thang, int nam)
        {
            if (QuyenAdmin())
            {
                return RedirectToAction("Index", "Home");
            }
            if (thang == 0)
            {
                ViewBag.Thang = "cả ";
            }
            else
            {
                ViewBag.Thang = "tháng " + thang;
            }
            ViewBag.Nam = nam;
            ViewBag.SelectNam = dao.ListDonHang().Where(x => x.ngayDatHang != null && x.ngayDatHang.Value.Year != nam).OrderByDescending(x => x.ngayDatHang).Select(x => x.ngayDatHang.Value.Year).Distinct();

            var model = dao.ReportDonHang(thang, nam).Where(x => x.trangThai.Contains("Đã nhận"));
            ViewBag.TongSanPhamTrongKho = dao.ListThongTinSanPham().Where(x => !x.tinhTrang.Contains("Đã bán")).Count();
            ViewBag.TongSanPhamDaXuat = model.Sum(x => x.tongSoLuong);

            ViewBag.TongGiaTriNhapVao = dao.TongGiaTriSanPham(thang, nam);
            ViewBag.TongGiaTriXuatRa = model.Sum(x => x.tongTien);
            ViewBag.TongGiaTriLoiNhuan = ViewBag.TongGiaTriXuatRa - ViewBag.TongGiaTriNhapVao;

            return View(model.ToList());
        }

    }
}