﻿using BanXeMoTo.Common;
using Models.DAO;
using Models.EF;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BanXeMoTo.Areas.Admin.Controllers
{
    public class NhanvienController : BaseController
    {
        private AdminDao dao = new AdminDao();

        // GET: Admin/Nhanvien
        public ActionResult Index(string searchString, int page = 1, int pagesize = 10)
        {
            if (QuyenAdmin())
            {
                return RedirectToAction("Index", "Home");
            }
            List<NhanVien> model;
            if (string.IsNullOrEmpty(searchString))
            {
                model = dao.ListNhanVien();
            }
            else
            {
                model = dao.ListNhanVien(searchString);
                if (model.Count == 0)
                {
                    SetAlert("Không tìm thấy " + searchString, "warning");
                }
            }
            ViewBag.Tong = model.Count();
            ViewBag.SoLuong = dao.ListNhanVien().Count();
            ViewBag.searchString = searchString;
            return View(model.ToPagedList(page, pagesize));
        }

        public ActionResult Create()
        {
            if (QuyenAdmin())
            {
                return RedirectToAction("Index", "Home");
            }
            var Listcv = dao.DanhSachChucVu();
            ViewBag.ListChucVu = new SelectList(Listcv, "maCV", "tenCV");
            ViewBag.MaNhanVien = dao.TaoMaNhanVien();
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(NhanVien model, int year, int month, int day)
        {
            if (QuyenAdmin())
            {
                return RedirectToAction("Index", "Home");
            }
            var Listcv = dao.DanhSachChucVu();
            ViewBag.ListChucVu = new SelectList(Listcv, "maCV", "tenCV");
            ViewBag.MaNhanVien = dao.TaoMaNhanVien();
            if (ModelState.IsValid)
            {
                model.maNV = ViewBag.MaNhanVien;
                model.nickName = "12345678";
                model.matKhau = Encryptor.EncryptMD5("12345678");
                DateTime ngaysinh = new DateTime(year, month, day);
                model.ngaySinh = ngaysinh;

                string result = dao.InsertNhanVien(model);
                if (result.Equals("done"))
                {
                    SetAlert("Đã tạo nhân viên mới thành công", "success");
                    return RedirectToAction("Details", new { @manhanvien = model.maNV });
                }
                else
                {
                    SetAlert(ThongBaoLoi(result), "danger");
                }
            }
            return View(model);
        }

        public ActionResult Details(string manhanvien)
        {
            if (QuyenAdmin())
            {
                return RedirectToAction("Index", "Home");
            }
            NhanVien model = dao.InfoNhanVien(manhanvien);
            if (model == null)
            {
                return RedirectToAction("Index");
            }
            return View(model);
        }

        public ActionResult Update(string manhanvien)
        {
            if (QuyenAdmin())
            {
                return RedirectToAction("Index", "Home");
            }
            NhanVien model = dao.InfoNhanVien(manhanvien);
            var Listcv = dao.DanhSachChucVu();
            ViewBag.ListChucVu = new SelectList(Listcv, "maCV", "tenCV");
            if (model == null)
            {
                return RedirectToAction("Index");
            }
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Update(NhanVien nhanvien, int year, int month, int day)
        {
            if (QuyenAdmin())
            {
                return RedirectToAction("Index", "Home");
            }
            var Listcv = dao.DanhSachChucVu();
            ViewBag.ListChucVu = new SelectList(Listcv, "maCV", "tenCV");
            DateTime ngaysinh = new DateTime(year, month, day);
            nhanvien.ngaySinh = ngaysinh;
            if (ModelState.IsValid)
            {
                NhanVien model = dao.InfoNhanVien(nhanvien.maNV);
                if (model == null)
                {
                    return RedirectToAction("Index");
                }
                nhanvien.nickName = model.nickName;
                //kiem tra mat khau co thay doi hay khong
                bool checkpass = dao.CheckPassNhanVien(model.maNV, nhanvien.matKhau);
                if (checkpass)
                {
                    nhanvien.nickName = nhanvien.matKhau;
                    nhanvien.matKhau = Encryptor.EncryptMD5(nhanvien.matKhau);
                }

                string result = dao.UpdateNhanVien(nhanvien);
                if (result.Equals("done"))
                {
                    SetAlert("Cập nhật nhân viên thành công!", "success");
                    return RedirectToAction("Details", new { @manhanvien = model.maNV });
                }
                else
                {
                    SetAlert(ThongBaoLoi(result), "danger");
                }
            }
            return View(nhanvien);
        }

        public ActionResult Khoataikhoan(string manhanvien)
        {
            if (QuyenAdmin())
            {
                return RedirectToAction("Index", "Home");
            }
            NhanVien model = dao.InfoNhanVien(manhanvien);
            if (model == null)
            {
                return RedirectToAction("Index");
            }
            bool result = dao.KhoaNhanVien(manhanvien);
            if (result)
            {
                SetAlert("Đã khóa tài khoản", "warning");
            }
            else
            {
                SetAlert("Khóa thất bại! Hãy kiểm tra lại thông tin", "danger");
            }
            return RedirectToAction("Details", new { manhanvien });
        }

        public ActionResult Huykhoataikhoan(string manhanvien)
        {
            if (QuyenAdmin())
            {
                return RedirectToAction("Index", "Home");
            }
            NhanVien model = dao.InfoNhanVien(manhanvien);
            if (model == null)
            {
                return RedirectToAction("Index");
            }
            bool result = dao.HuyKhoaNhanVien(manhanvien);
            if (result)
            {
                SetAlert("Đã mở khóa tài khoản", "success");
            }
            else
            {
                SetAlert("Mở khóa thất bại! Hãy kiểm tra lại thông tin", "danger");
            }
            return RedirectToAction("Details", new { manhanvien });
        }

    }
}